const fs = require('fs');

let raw = fs.readFileSync(__dirname + '/../extract.rsc', "utf-8");


const iRegs = [
  { param: 'name', reg: /name=(.*)/i},
  { param: 'password', reg: /password=(.*)/i},
  { param: 'remote-address', reg: /remote-address=(.*)/i},
]
// We only match if last line is not preceed by \
const sReg = /(?<!\\)\r\n/;
const output = [];
while (raw.length > 0) {
  const i = raw.search(sReg);
  // We also take the delimeters
  const line = raw.slice(0, i + 2);
  const entry = {}
  iRegs.forEach((item) => {
    const match = line.match(item.reg);
    if (match) {
      const j = match[1].search(/\s/);
      // We take only the word before space
      entry[item.param] = match[1].slice(0, j);
    }
  })
  output.push(entry);
  raw = raw.slice(i + 2);
}

fs.writeFileSync('output.json', JSON.stringify(output));
