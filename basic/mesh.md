<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Mapes de les xarxes mesh](#mapes-de-les-xarxes-mesh)
- [qMp](#qmp)
  - [No puc accedir per IPv4 al qMp](#no-puc-accedir-per-ipv4-al-qmp)
  - [Analitzar tràfic wireshark de forma còmode](#analitzar-tr%C3%A0fic-wireshark-de-forma-c%C3%B2mode)
  - [Obtenció del firmware](#obtenci%C3%B3-del-firmware)
- [Comanda útil per valorar qualitat del wifi en context d'instal·lació](#comanda-%C3%BAtil-per-valorar-qualitat-del-wifi-en-context-dinstal%C2%B7laci%C3%B3)
  - [Més sobre la comanda](#m%C3%A9s-sobre-la-comanda)
  - [Sortida de la comanda](#sortida-de-la-comanda)
  - [Tractament en cas de múltiples enllaços](#tractament-en-cas-de-m%C3%BAltiples-enlla%C3%A7os)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Mapes de les xarxes mesh

- http://dsg.ac.upc.edu/qmpmon/
- http://sants.guifi.net/maps/
- http://libremap.net/

# qMp

## No puc accedir per IPv4 al qMp

Requeriments: accés a l'antena a través de cable (capa dos, link layer). És a dir, tenim un cable al portàtil i a l'altre banda hi ha l'antena o un switch fins l'antena (cap router enmig).

Si cumplim els requeriments podrem fer servir la IPv6 link local per accedir a l'antena i reiniciar-la. Per a tals efectes, hem d'estar connectats a l'antena per IPv6 (el network manager té una opció de link-local only), també va bé un altre intent de connexió habitual IPv4, però es pitjor ja que quan arribi el timeout ens desconnectarà.

```
ping6 ff02::1%eth0
PING ff02::1%wlan0(ff02::1) 56 data bytes
64 bytes from fe80::a8f0:a718:5962:b53: icmp_seq=1 ttl=64 time=0.040 ms
64 bytes from fe80::8fff:d3e:9b72:dcb4: icmp_seq=1 ttl=64 time=2.58 ms (DUP!)
64 bytes from fe80::a8f0:a718:5962:b53: icmp_seq=2 ttl=64 time=0.071 ms
64 bytes from fe80::8fff:d3e:9b72:dcb4: icmp_seq=2 ttl=64 time=2.83 ms (DUP!)
64 bytes from fe80::a8f0:a718:5962:b53: icmp_seq=3 ttl=64 time=0.084 ms
64 bytes from fe80::8fff:d3e:9b72:dcb4: icmp_seq=3 ttl=64 time=2.98 ms (DUP!)
```

La resposta DUP ens diu que hi ha diversos dispositius que han respost al pong del ping. En aquest cas han sigut 2 (dos IPv6 diferents), i mínim seran 2: nosaltres i l'altre extrem. Agafem la IPv6 del dispositiu que ha fet més latència, vol dir que és l'altre banda.

Llavors ja ens podem connectar a l'antena per ssh:

`ssh root@fe80::8fff:d3e:9b72:dcb4%eth0`

Podem intentar recuperar l'accés de diferents maneres. La més extrema consisteix a borrar el següent fitxer. Això provocarà que l'antena després de reiniciar-se torni als paràmetres per defecte:

`rm /qmp_configured`

reiniciem:

`reboot`

## Analitzar tràfic wireshark de forma còmode

Normalment el procés per capturar tràfic és:

1. Fer ssh a la màquina
2. Capturar amb tcpdump en un fitxer
3. Moure el fitxer a l'ordinador local amb scp

Hi ha però una manera de fer-ho només amb una comanda:

`ssh root@<ip> tcpdump -U -s0 -w - 'not port 22' | wireshark -k -i -`

src http://www.commandlinefu.com/commands/view/4373/analyze-traffic-remotely-over-ssh-w-wireshark

## Obtenció del firmware

per binari o compil·lació

```
The qMp 3.2.1 firmware binaries can be found at http://fw.qmp.cat/Releases/3.2.1. Otherwise, you can compile your own images with your preferred options:

git clone git://qmp.cat/qmpfw.git qmpfw-3.2.1
cd qmpfw-3.2.1
git fetch --tags
git checkout tags/v3.2.1
git checkout -b v3.2.1
QMP_GIT_BRANCH=v3.2.1 make checkout
cd build/qmp && git checkout -b v3.2.1 && cd ../..
make J=n T=target

where n is the number of parallel threads to use in the compilation (1, 2, ..., 8...) and targets is the name of the device target to build the image (list them by issuing the command make list_targets).
```

# Comanda útil per valorar qualitat del wifi en context d'instal·lació

`while true; do clear; iw dev wlan0 station dump; sleep 1; done`

Vagi a la subsecció: Més sobre la comanda, per a més informació.

Cada segon obtindrem la següent sortida, pot veure-la a la subsección: Sortida de la comanda.

Resulta útil per 2 escenaris que formen part de l'instal·lació guifi d'un node:

- Alineament de l'antena: Consisteix a moure l'antena del node de guifi per tal d'obtindre el millor enllaç possible. Amb el mètode típic des de qMp s'ha d'esperar 5 seguns aproximadament, i no aconseguíem, per exemple, millor de -81 dBm. Amb aquest mètode vam poder afinar fins -74 dBm. També és una forma de controllar l'equilibri entre el RX i TX (recepció i transmissió). Estic parlant especialment pel cas d'un node precari: és un node el qual no té visibilitat directa, però es beneficia dels rebots.
- Prova de cobertura del punt d'accés a l'espai. La mateixa comanda en l'ordinador portàtil (si tenim GNU/Linux) i en moviment, ens permetrà verificar àgilment la qualitat de la cobertura del punt d'accés wifi a l'espai en qüestió. I podrem verificar quin és el millor lloc per posar el punt d'accés wifi o si en calen més.

## Més sobre la comanda

escrit de forma clara:

```
while true; do
    clear
    iw dev wlan0 station dump
    sleep 1
done
```

while és una estructura de control que executa un codi donada una condición. En aquest cas la condició sempre serà certa, per tant, s'executarà indefinidament fins que es pari manualment (Control+C).

I què executa? Per començar `clear` neteja la pantalla. Després la comanda `iw dev wlan0 station dump` ens dona informació rellevant sobre com estem associats. Segueix `sleep 1` com a pausa per executar la comanda cada segon. `done` posa fi a l'estructura de control.

## Sortida de la comanda

```
Station 12:34:56:78:9a:bc (on wlan0)
    inactive time: 60 ms
    rx bytes: 25495350
    rx packets: 109678
    tx bytes: 1535773
    tx packets: 10623
    tx retries: 10359
    tx failed: 0
    signal:   -77 dBm
    signal avg: -76 dBm
    tx bitrate: 26.0 MBit/s MCS 3
    rx bitrate: 43.3 MBit/s MCS 4 short GI
    authorized: yes
    authenticated: yes
    preamble: long
    WMM/WME: yes
    MFP: no
    TDLS peer: no
```

## Tractament en cas de múltiples enllaços

Si veus més d'un enllaç i vols concentrar-te en un concret llavors

```
station="00:00:00:00:00:00"
while true; do clear; iw dev wlan0 station dump | grep "$station" -A 25; sleep 1; done
```
