articles in newspaper (news/press), blogs, instutions, etc.

as they are external references at least are saved in https://web.archive.org

# internal

content promoted from people involved in community networks

- https://pillku.org/article/las-redes-libres-y-la-ipv6-como-medio-para-llegar-/ (accessed 2018-05-15)
- https://pillku.org/pillku-edicion-antologia.pdf (accessed 2019-01-13)

# external

content promoted from people not directly involved in community networks

## direct mention

- http://opcions.org/revista/num-50-la-petjada-lera-digital/ - Donar connexió a Internet, a tothom i a totes les coses (accessed 2019-5-5)
- https://findingctrl.nesta.org.uk/text/ - Ordinary people are increasingly reclaiming control. Communities from Catalunya to Detroit are building their own local internet networks (accessed 2019-5-1)
- https://www.internetsociety.org/resources/2018/unleashing-community-networks-innovative-licensing-approaches/ (accessed 2018-05-15)
- https://sloanreview.mit.edu/projects/seven-technologies-remaking-the-world/#chapter-3 - as the digital revolution rages on, every business leader must become technology literate. This guide provides executives with an introduction to the technologies that are transforming our world (accessed 2018-05-15)
- https://startupscolaborativas.com/guifi-net-mayor-red-telecomunicaciones/

## indirect mention

- https://www.elconfidencial.com/tecnologia/2018-03-28/evgeny-morozov-uber-airbnb-rusia-putin-cambridge-analytica_1542121 - Es la infraestructura lo que hay que socializar, no los datos. Sin la infraestructura, los datos no valen para nada. (accessed 2018-05-15)
