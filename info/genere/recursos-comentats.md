### Llenguatge inclusiu

Us compartim un parell de recursos per ajudar-nos amb el tema del **llenguatge inclusiu**, del mogollón que hi ha publicats:

- Extens, amb molts exemple i moltes propostes d'ús alternatiu al masculí: https://oficinaigualtat.uib.cat/digitalAssets/297/297565_marcar-les-diferencies.-institut-catla-de-la-dona.pdf

- De consulta més àgil ja que és una adaptació ràpida d'un material més ampli: https://www.ub.edu/cub/guiarapida.php?id=2510 (guia completa aquí: https://www.ub.edu/cub/criteri.php?id=2510)

- Específic per a mitjans de comunicació: https://oficinaigualtat.uib.cat/digitalAssets/297/297561_llenguatge-per-la-igualtat-als-mitjans-de-comunicacio.pdf

En CASTELLANO:

- Extenso y argumentado: https://oficinaigualtat.uib.cat/digitalAssets/297/297597_643228-guia_sindical_para_un_uso_del_lenguaje_no_sexista.pdf

- Quizá demasiado resumido: https://oficinaigualtat.uib.cat/digitalAssets/297/297600_guia-del-lenguaje-no-sexista-universidad-de-granada.pdf
