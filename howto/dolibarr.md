# DOLIBARR

## Documentació
https://wiki.dolibarr.org/index.php/Main_Page

## Instal·lació
### Dependencies
Paquets a instal·lar (ja instal·lats al server exo):
postgresql apache2 php-zip php-json php-xmlrpc php php-common php-xml php-cli wget unzip

Nous, no estaven instal·lats:
php-json php-xmlrpc php-gd php-zip postgresql php-pgsql
php-fpm php-soap php-intl php libapache2-mod-php php-curl php-intl php-mbstring php-soap

### Configuració postgresql
Per accedir al shell de postgres:
sudo -u postgres psql

Funcions bàsiques dins de psql:
    \h      help
    \list   llistat de bdd existents

Totes les comanes van amb majúscules i acaben en ;
Creació de l’usuari;
CREATE USER dolibarr with encrypted password 'dolibarr’;

Creació de la bdd:
CREATE DATABASE dolibarr encoding 'UTF8' owner dolibarr;

Ctrl d per sortir de la sessió

### Sentencies habituals

Per accedir a la bdd:
psql -U dolibarr -d dolibarr -h localhost

SELECT * FROM llx_c_email_templates WHERE rowid = 9;
SELECT topic, content FROM llx_c_email_templates WHERE rowid = 9;
UPDATE llx_c_email_templates SET module = 'sendrecurringinvoicebymail.old' WHERE rowid = 9;

Canviar de nom la bdd:
ALTER DATABASE dolibarr RENAME TO dolibarrini;

Per accedir a la bdd:
sudo -u postgres psql -d dolibarr

Per veure les taules:
    \dt per llistat de taules


Per modificar un registre d’una taula:
update taula set camp=’nou valor’ where campCriteri = ‘criteri;

Per llistar registres d’una taula:
SELECT * FROM taula;
SELECT camps FROM taula WHERE index > n;


### Bdd backup i restauració

https://wiki.dolibarr.org/index.php/Backups
#### Backup

Des d’un shell:
su
pg_dump -F p -h localhost -U dolibarr -p 5432 -f "/var/lib/dolibarr/documents/admin/backup/pg_dump_dolibarr_10.0.3_20200218.sql.gz" -Z 9 -w dolibarr -W

#### Restauració

descomprimir el fitxer amb gzip -d nom_fitxer

psql  -d dolibarr -h localhost -p 5432 -U dolibarr -W -f data

El procediment demanarà el passwd
Ignorar el error:
psql:data:30: ERROR:  must be owner of extension plpgsql

## Instal·lació del paquet Dolibarr
Descarga
https://sourceforge.net/projects/dolibarr/files/

wget https://downloads.sourceforge.net/project/dolibarr/Dolibarr%20installer%20for%20Debian-Ubuntu%20%28DoliDeb%29/10.0.3/dolibarr_10.0.3-4_all.deb
Instal·lació
apt install ./paquet
El directori per defecte es Directory /usr/share/dolibarr/htdocs

Base de dades:
Usarem postgreSQL
Crearem una bdd buida amb nom dolibarr

per la instal·lacio final accedir a localhost/dolibarr/install
En la definició de la bdd no marcar la creacio de la bdd ni el usuari ja que estarà creat previament

Parametres de configuració:
    - Idioma: catala
    - Nom bdd: dolibarr
    - Tipus bdd: postgresql
    - Port: 5432
    - Login bdd: dolibarr
    - Pass bdd: pass

## Apache
La configuració es guarda a /etc/apache2/conf-available
Assegurar que apache te activat el mòdul de php

Aplicar els canvis: systemctl reload apache2

## Procediments per configurar Dolibarr

### Configurar empresa/organització

### Mòduls
Socis: activar email obligatori des d’opcions del mòdul
Tercers
Pressupostos
Comanda de vendes
Intervencions
Tiquets
Proveïdors
Factures
Impostos
Bancs
Domiciliacions
Comptabilitat simplificada
Serveis
Projectes
Agenda
Sistema de gestió documental
Generador d’aplicacions
Importació de dades
Exportació de dades
Notificacions sobre esdeveniments comercials -> per enviar els correus associats a factures
E-Mailings massiu
Serveis API/WEB (servidor REST)
LDAP
Tasques programades
ISP

### Ajustos de dades
A configuració -> diccionari definir:
Títol de civisme crear Sr i Sra i desactivar la resta

A Socis -> Tipus de socis
    1    si     si    Soci amb Internet
    2    si     si    Soci sense Internet


### Importacions

Per importar fitxers:
Format de la data yyyy-mm-dd (english EEUU)

S’ha de fer sempre com csv ja que amb excel no entén les dates

Totes les importacions es realitzen des del mòdul d’importacions.
#### Socis
Previ a importar cal definir els tractaments del socis i els tipus de socis.

Importar socis. S’exporta des de la línia 2 per descartar la capçalera
Usar la plantilla guardada per l’associació de camps

#### Tercers
Sols es necessita crear per temes de facturació. Usuaris de baixa ja no calen.
Importar Tercers -> Usuaris i les seves propietats
Importar els tercers des de les dades bancaries adeudos.xml. S’obre amb excel com xml table.
El codi client es CU1911-000xx on xx es un codi seqüencial. Per saber si es CU1911 és crea un tercer a mà i es mira quin codi assigna. Després s’esborra per no duplicar-lo.
Sols crearem els tercers que tinguin domiciliació

Després s’ha de associar a mà des de l’aplicació els socis i els tercers.
La millor manera és des del llistat de socis buscar el primer (josep Mercader), entrar a modificar, seleccionar la pestanya afiliacions i Enllaç tercer Dolibarr.
Per trobar més ràpid es pot anar escrivint el nom
Per canviar de soci, a dalt a la dreta i hi ha els cursors
Botó modificar per salvar.

#### Comptes bancaris
Sols s’exporten els que consten en el mandat sepa adeudos.
Per aconseguir el codi client, exportem els tercers i eliminem els que no tenen compte corrent
No importar l'últim camp
Serveis i productes
Crear els serveis de les quotes de soci
Altres serveis o productes

#### Banc
Gestió dels comptes i domiciliacions
L’ultim camp no s’exporta
La data en format americà
El nom del socis com la llista, no com les dades bancaries

## Desenvolupar mòdul

Primer, des de Configuració -> Altres configuracions 
    MAIN_FEATURES_LEVEL 2

A dalt a la dreta apareixerà l’ icona d’una marieta
Crearem el modul ServeisIT 

Editarem el fitxer serveisit/core/modules/modServeisIT.class.php des de la propia pagina canviant:
    $this->numero = 500000; a $this->numero = 500333;
Si finalment fem public aquest modul caldria demanar un rand de numeració per la eXO.

Taules, directoris i fitxers

Taules que es creen:
IIx_nomModul_xxx

Taules amb registres:
llx_cronjob
llx_const
llx_menu
llx_rights_def

Directoris:
Els moduls es creen a : dolibarr/htdocs/custom/nomModul

Fitxers:
ispindex.php

core/modules/modISP.class.php

Menus
Els menus de l’esquerra estan a llx_menu

Titols
Els titols estan als fitxers php de menus
Es pot saber quin fitxer s’executa en el registre del menu

serveisclient_list.php (pag llistat serveis)
Buscar title
es la primera entrada que es troba

serveisclient_list.php  (pag de Alta de nou servei)
llxHeader('', $langs->trans('Servei Client'), '');  -> es el caption
print load_fiche_titre($langs->trans("NewObject", $langs->transnoentitiesnoconv("Servei Client")));  -> es el titol

## email per factures recurrents
Carregar el mòdul:
https://code.bugness.org/Dolibarr/sendrecurringinvoicebymail/src/branch/master

El text  del correu es trova en la bdd, taula llx_c_email_templates a on module = 'sendrecurringinvoicebymail'

A utilitats -> plantilles correu 
crear una plantilla asociada a la generació de factura
Asumpte: __MYCOMPANY_NAME__ - Nova factura: __REF__

Adjuntar fitxer: 1
Contingut:
Benvolgut soci,<br><br>
Adjunt trobaràs la factura __REF__ que es carregarà próximament en el teu compte.<br><br>

Cordialment,<br>
El sistema automatitzat de facturació de la __MYCOMPANY_NAME__

Despres esborrar el registre de plantilles de la bdd amb module = 'sendrecurringinvoicebymail'

i el nou registre canviar module = 'sendrecurringinvoicebymail'
## Actualitzacions
Descarregar sempre el paquet per debian no el zip
sudo apt install paquet



## Disaster recovery

### Dades necesaries

En el servidor, crear un tar del directori custom amb tots els paquests instal·lats
tar zcvf isp.tar /usr/share/dolibarr/htdocs/custom/

Cal tenir una backup de la bdd

### Restauració
Per restaurar cal:
Instalar des de cero dolibarr
Entrar fins posar el pasword del admin
Esborrar bdd i tornar a crear en blanc
Restaurar la bdd des del backup
Extreure el tar del directori custom des de l’arrel
canviar la propietat de tots els fitxers i directoris de isp a www-data.www-data

## Operació

### Dades basiques

Afegir les dades de l’associació a Inici -> configuració -> Empresa/organització
Afegir el compte bancari a Banc

### Facturació
Factura puntual

Anar a financera -> Nova factura

Seleccionar el client
Indicar a data de la factura
condicions de pagament-> a la recepció
si correspon a un projecte, indicar el projecte
Seleccionar la plantilla per defecte
En nota publica, afegir una descripció
Crear l’esborrany
Afegir les línies de la factura. Pot ser a partir de serveis definits o lliure

Un cop afegides les línies, validar la factura
Factura periódica

Des de la plantilla, crear com factura repetitiva i marcar la periodicitat

### Correu
Configuració del correu

Inici -> configuració -> correu
Mètode d'enviament de correu electrònic: Swift Mailer socket library

Configuració de l’enviament automàtic

/*
A utilitats -> plantilles correu 
crear una plantilla asociada a la generació de factura
A Mòduls -> Configuració del mòdul de notificació per correu electrònic
en Llista de notificacions fixes automàtiques
configurar Factura BILL_VALIDATE amb el correu de la exo i el valor > 0
*/


### cron dolibarr

com a administrador fes `crontab -e`

i afegeix:

    */5 * * * * /usr/share/dolibarr/scripts/cron/cron_run_jobs.php <securitykey> <login_user> > /var/lib/dolibarr/documents/cron_run_jobs.php.log

### Notes adicionals

- a data de 2020-3-22:
  - a efectes d'estabilitat, dolibarr està més provat amb base de dades mysql/mariadb que postgresql
  - les operacions de base de dades van directes sense fer ús d'un [ORM](https://en.wikipedia.org/wiki/Object-relational_mapping)
