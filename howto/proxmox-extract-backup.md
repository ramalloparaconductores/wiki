# proxmox extract backup

## get raw image from vma.lzo file (proxmox backup)

uncompress (assuming only one lzo file in the directory):

    lzop -d vzdump-qemu-*.vma.lzo

extract files inside (assuming only one vma file in the directory):

    vma extract *.vma -v /tmp/vmtest

the disk is in raw format and ready to use in standard qemu, you can run it with something like:

    myfile=/path/to/your/raw/file
    qemu-system-x86_64 -enable-kvm -localtime -m 2G -vga std -netdev user,id=wan -device virtio-net,netdev=wan,id=nic1 -drive file=$myfile,cache=none,if=virtio

## use debian to extract it

use `vma` utility in a debian without proxmox. Understand why is not there reading this forum thread https://forum.proxmox.com/threads/vma-archive-restore-outside-of-proxmox.14226/page-2

add proxmox repo

    echo "deb [arch=amd64] http://download.proxmox.com/debian stretch pve" >> /etc/apt/sources.list.d/pve.list

download gpg key -> src https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_Stretch

    wget http://download.proxmox.com/debian/proxmox-ve-release-5.x.gpg -O /etc/apt/trusted.gpg.d/proxmox-ve-release-5.x.gpg
    chmod +r /etc/apt/trusted.gpg.d/proxmox-ve-release-5.x.gpg  # optional, if you have a changed default umask

update repos

    apt update

dependencies

    apt install libiscsi7 glusterfs-common libglib2.0-0 libiscsi1 librbd1 libaio1 lzop

avoid using proxmox packages and its updates: limit proxmox repositories -> thanks https://wiki.debian.org/DebianRepository/UseThirdParty#Standard_pinning

in `/etc/apt/preferences.d/limit-pve` put:

```
Package: *
Pin: origin download.proxmox.com
Pin-Priority: 150
```

as user, download deb

    apt download pve-qemu-kvm

get the binary (assuming only one version of `pve-qemu-kvm` in that directory):

    dpkg --fsys-tarfile ./pve-qemu-kvm_*_amd64.deb | tar xOf - ./usr/bin/vma > ./vma

put `vma` file reachable for your `PATH`

extra note: I used this source but the dependencies are old -> src https://www.liberasys.com/wiki/doku.php?id=proxmox-extract-backup:proxmox_extract_backup
