# Configuració de Mikrotik RBD52G-5HacD2HnD per Sarenet

S'ha de tenir accès a Internet.

Endollar i accedir a 192.168.88.1

La primera vegada accedeix automàticament a la configuració **[Quick Set]**

## Firmware ##

Comprobar la versió del firmware.

![Imatge](img-doc/mk01.png)

Per instal.lar la recomanada (actualment: v6.46.1 (stable)):

Desde la mateixa pàgina de **[Quick SeT]** a l’apartat **System** a **[Check for Updates]** accedirem a instal.lar el nou firmware:

![Imatge](img-doc/mk02r.png)

Aquest procès finalitza en uns minuts i cal tornar a entrar.

## Password i Identificació ##

Entrarà aut. en la mateixa pantalla **[Quick Set]** aprofitarem i posarem el password.

Identificarem que el n/s i les MAC Address estiguin correctes en la base del router:

![Imatge](img-doc/mk05s.png)

El n/s el trobarem a **System**/**License**

![Imatge](img-doc/mk03.png)

Les MAC a **Interfaces**:

![Imatge](img-doc/mk04.png)

La MAC de la wlan2 la farem servir per identificar el router: a **System**/**Identity**

![Imatge](img-doc/mk06.png)

## Wifis ##

Afegim una password general per la wifi: a **Wireless**/**Security Profiles**

![Imatge](img-doc/mk07.png)

![Imatge](img-doc/mk08.png)

Configurem les wifis (**Advanced Mode**) amb:
- SSID: eXO_nnnnnn_2G (i5G) a on nnnnnn son el 6 ult. Digits de la MAC respectiva
- Wireless Protocol: 802.11 (*fins ara venia per defecte: any*)
- Security profile: el que acaben de crear
- Frequency Mode: regulatory-domain
- Country: spain
- Antenna Gain: 3 dBi
- Distance: indoors (*fins ara venia per defecte: dynamic*)

## Bridges ##

Porta per defecte un bridge que modificarem de nom a «**lan**» per diferenciar-lo de l’altre bridge que crearem.

Creem un nou bridge: «**bstrm**» amb els valors per defecte.

## Interfaces ##

Creem una nova interficie tipus VLAN:
- Name: `ether1.24`
- VLAN ID: 24
- Interface: `ether1`

Un cop creada ja la podem afegir a un nou Port al bridge "bstrm"

Crearem dues noves interfaces tipus PPPoE:

Una tipus PPPoE Client:
- Name: `exo`
- Interfaces: `bstrm`
- User: `id_user@exo.cat`
- Password: laquesigui
- Profile: Default
- Keepalive Timeout: 30
- Use Peer DNS: yes
- Add Default Route: yes

Un altre per gestió, tipus PPPoE Client:
- Name: `mngt`
- Interfaces: `bstrm`
- User: `m_user@exo.cat`
- Password: la mateixa del user
- Profile: default
- Keepalive Timeout: 30
- Use Peer DNS: no
- Add Default Route: no

## Prioritat ##

Hem d’afegir prioritat, es fa a nivell de filtre pel bridge "bstrm"

![Imatge](img-doc/mk11.png)

Nou filtre:
- Chain: output
- Bridges / Out. Bridge: `bstrm`
- Action: set priority
- New Priority: 3

## Interface List ##

Comprobem i modifiquem a **Interfaces** / **Interface List**:

![Imatge](img-doc/mk12.png)

De forma que quedi:

![Imatge](img-doc/mk13.png)

## IPv6 ##

Cal assegurar-se que el paquet esta instal.lat i activat

![Imatge](img-doc/mk19.png)

Si no es així requereix un reboot per activar-lo

![Imatge](img-doc/mk19b.png)

Crear a **IPv6** un nou **DHCP Client** amb els valors:

- Interface: `exo`
- Request: prefix
- Pool Name: exo-prefix
- Pool Prefix Lengt: 56
- Prefix Hint: ::/0

![Imatge](img-doc/mk16.png)

Un cop activat el DHCP Client ipv6 es crearà aut. un **Pool** que s'utilitzarà per crear la nova adreça a **Addresses**

En la nova adreça relacionarem el pool creat i la interface «lan»

![Imatge](img-doc/mk15.png)

( *completar backup i export, i proves* )
