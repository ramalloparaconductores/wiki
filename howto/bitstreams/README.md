# Relació de configuracions

- Per routers i per majoristes

## Mikrotik haPac2 per Sarenet

[mksarenet.md](mksarenet.md) Configuració **MiKrotik haPac2** per sarenet

[mkconfboto.md](mkconfboto.md) Configuracio del **boto** d'usuari en Mikrotik haPac2 per **wifi on/off**

### FreeBSD per Sarenet

1) https://evilham.com/en/blog/2019-FreeBSD-eXO-router/

2) https://evilham.com/en/blog/2020-FreeBSD-home-router-ipv6/

3) https://evilham.com/en/blog/2020-FreeBSD-home-router-legacy-ipv4/

### Arxius de configuració de referència

En el repositori [mk-bstrm-config](https://gitlab.com/guifi-exo/mk-bstrm-config)
podeu trobar una referència actualitzada de les configuracions aplicades.
