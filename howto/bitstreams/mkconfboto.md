# Configuració del botó d'usuari per apagar/engegar la wifi

Mikrotik RBD52G-5HacD2HnD

![Imatge](img-doc/hAPac2.png)

Tret i provat del repositori [mk-bstrm-config](https://gitlab.com/guifi-exo/mk-bstrm-config)

Probablement hi ha una solució per pujar un fitxer i modificar la configuració com apareix en el doc. anterior
***Aqui esta explicat manualment:***

## Definim el led d'usuari
Que indicara la execució correcta del script

A **System** / **Leds** definim un nou:

![Imatge](img-doc/mk18b.png)

## Creem un script
A **System** / **Scripts** Creem un de nou amb els valors:

![Imatge](img-doc/mk18.png)

- Name: `wifi_onoff`
- Policy: `read, write`
- Source:
```
:log info "Button pushed"

:foreach i in=[/interface wireless find] do={
:local IfaceName [/interface ethernet get $i name]

:if ([/interface get $i disabled]) do={
/interface ethernet set $i disabled=no
/system leds set [find where leds="user-led"] type=on
:log info "Interface wireless $IfaceName enabled"
} else={
/interface ethernet set $i disabled=yes
/system leds set [find where leds="user-led"] type=off
:log info "Interface wireless $IfaceName disabled"
}
}
```

## Definim el botó en relació al script:

A **System** / **Routerboard** (activat) / ** Mode Button **
- `Enabled` (activat)
- On event: `wifi_onoff`

Podem en la mateixa pagina del script fer proves i consultar a **Log**

## Afegir l'explicació "automàtica" pujant un fitxer i executant-lo desde el terminal
