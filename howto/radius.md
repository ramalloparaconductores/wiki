this guide installs daloradius as frontend (WebGUI) to use freeradius backend

start with guide https://kifarunix.com/install-lamp-stack-on-debian-9/

update and upgrade as necessary

    apt update
    apt upgrade

install webserver

    apt install apache2

[listen appropriate interfaces](https://httpd.apache.org/docs/2.4/bind.html) in `/etc/apache2/ports.conf`

install mariadb

    apt install mariadb-server

install php modules

    apt install php libapache2-mod-php php-mysql php-common php-gd php-mbstring php-curl php-xml

continue with guide https://kifarunix.com/install-freeradius-with-dolaradius-on-debian-9/

install php extensions

    apt install php-mail php-mail-mime php-pear

Install [PHP Pear DB library](https://stackoverflow.com/questions/1959925/what-is-pear-db-library)

    pear channel-update pear.php.net
    pear install DB

note: DB is required by daloradius, in the future could be MDB2

install freeradius

    apt-get install freeradius freeradius-mysql freeradius-utils --no-install-recommends

on `/etc/freeradius/3.0/sites-enabled/default` (check lines 117, 179, 219) select the particular network interface you want to listen (restrict request to the networks you trust), and then

    service freeradius restart

use this script to create a user and database (first 2, then 6)

    mysql_setpermission

populate radius database and enter radius_user_password

    mysql -u radius_user -p radius_db < /etc/freeradius/3.0/mods-config/sql/main/mysql/schema.sql

Enable FreeRADIUS SQL

    ln -s /etc/freeradius/3.0/mods-available/sql /etc/freeradius/3.0/mods-enabled/

Edit `/etc/freeradius/3.0/mods-enabled/sql` and change dialect from `sqlite` to `mysql` put user and passwords appropriately and uncomment option `read_clients = yes`

Install unzip

    apt install unzip

Prepare daloradius

    wget https://path/to/daloradius-1.0-0.zip
    unzip daloradius-1.0-0.zip
    mv daloradius-master /var/www/html/daloradius

Prepare database

    mysql -u radius_user -p radius_db < /var/www/html/daloradius/contrib/db/fr2-mysql-daloradius-and-freeradius.sql
    mysql -u radius_user -p radius_db < /var/www/html/daloradius/contrib/db/mysql-daloradius.sql

Adjust permissions

    chown -R www-data.www-data /var/www/html/daloradius/
    chmod 664 /var/www/html/daloradius/library/daloradius.conf.php

Adjust daloradius config in `/var/www/html/daloradius/library/daloradius.conf.php`. With `msqli` and appropriate users and passwords for accessing the sql database, and `$configValues['FREERADIUS_VERSION'] = '3';`

restart freeradius

    systemctl restart freeradius

access daloradius in http://my_ip/daloradius - It is recommended protecting this site through VPN access and that only is accessed through admin LANs or VLANs

Other resources:

- https://www.grenode.net/Documentation_technique/R%C3%A9seau/Collecte_xDSL/
- https://www.ffdn.org/wiki/doku.php?id=formations:radius&s[]=radius
- https://github.com/lirantal/daloradius/blob/master/INSTALL
- http://debian-facile.org/atelier:chantier:installation-freeradius-daloradius-sous-debian-9.4.0
