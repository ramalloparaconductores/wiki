<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [upgrade cacti from 0.8.8h to 1.2.1 (latest) in debian 9](#upgrade-cacti-from-088h-to-121-latest-in-debian-9)
  - [cacti](#cacti)
  - [spine](#spine)
  - [interesting utilities found](#interesting-utilities-found)
- [upgrade cacti from older debian to new debian](#upgrade-cacti-from-older-debian-to-new-debian)
- [official documentation reference](#official-documentation-reference)
- [extra recommended template for graphs](#extra-recommended-template-for-graphs)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# upgrade cacti from 0.8.8h to 1.2.1 (latest) in debian 9

a way to control cacti version through the compilation of git repos

## cacti

build cacti

    packagelist=(
        git # to clone source repositories
        cacti # use defaults user and password cacti
        php7.0-gd # seems this is not included but is need by the installation
        postfix # local configuration is OK (cacti expects sendmail binary)
        php7.0-sqlite3 # looks like is required according to cacti logs
    )

    apt-get install ${packagelist[@]}
    cd /usr/local/src
    git clone https://github.com/Cacti/cacti
    cd cacti
    git checkout release/1.2.3

adapt location in apache config file in `/etc/apache2/conf-enabled/cacti.conf`

```diff
-Alias /cacti /usr/share/cacti/site
+Alias /cacti /usr/local/src/cacti

-<Directory /usr/share/cacti/site>
+<Directory /usr/local/src/cacti>
```

linking config file to cacti repo

    cd /usr/local/src/cacti/include/
    mv config.php config.php.bak
    ln -s /etc/cacti/debian.php config.php
    # database_type must be mysql, with mysqli you will have a FATAL error cannot connect to DB
    sed -i 's/$database_type = "mysqli"/$database_type = "mysql"/' config.php

adaptations that must be done to continue with install/migration. in `/etc/php/7.0/apache2/php.ini` (all these values are in the config, find them and adapt):

    date.timezone = "Europe/Madrid"
    memory_limit = 450M
    max_execution_time = 60

adaptations that must be done to continue with install/migration. in `/etc/php/7.0/cli/php.ini` (all these values are in the config, find them and adapt):

    date.timezone = "Europe/Madrid"

restart apache2

    service apache2 restart

according to https://github.com/Cacti/cacti/issues/361, this is required:

    mysql -u root
    use mysql
    GRANT SELECT ON mysql.time_zone_name TO cacti@localhost;
    flush privileges

and put data on it

    mysql -u root mysql < /usr/share/mysql/mysql_test_data_timezone.sql

and put cacti profiles

    mysql cacti < /usr/local/src/cacti/cacti.sql

requirements that installation says

    apt install php7.0-gmp php7.0-ldap # needed to install these two (may vary your situation)

restart to count with that dependencies

    service apache2 restart

performance improvements suggested by cacti install `/etc/mysql/my.cnf`

```conf
[mysqld]
max_heap_table_size=49M
max_allowed_packet=16777216
tmp_table_size=64M
join_buffer_size=64M
innodb_buffer_pool_size=500M
innodb_doublewrite=OFF
innodb_additional_mem_pool_size=80M
innodb_flush_log_at_timeout=3
innodb_read_io_threads=32
innodb_write_io_threads=16
character-set-server=utf8mb4
collation-server=utf8mb4_unicode_ci
```

do database collation:

    mysql -e 'ALTER DATABASE cacti CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;'

restart service to apply changes

    service mariadb restart

(optional - useful if you have data to migrate) move graph data

    cp -pa /var/lib/cacti/rra/* /usr/local/src/cacti/rra/

update `/etc/cron.d/cacti` info:

```diff
-*/5 * * * * www-data php --define suhosin.memory_limit=512M /usr/share/cacti/site/poller.php 2>&1 >/dev/null | if [ -f /usr/bin/ts ] ; then ts ; else tee ; fi >> /var/log/cacti/poller-error.log
+*/5 * * * * www-data php --define suhosin.memory_limit=512M /usr/local/src/cacti/poller.php 2>&1 >/dev/null | if [ -f /usr/bin/ts ] ; then ts ; else tee ; fi >> /var/log/cacti/poller-error.log
```

check /etc/cacti/spine.conf DB_pass matches the config.php

## spine

we require to use the same version of cacti and spine (as stated [here](https://forums.cacti.net/viewtopic.php?f=21&t=56943)). both versions of spine cannot coexist (or it would be tricky because the path search cacti does in poller.php)

    sudo apt remove cacti-spine

build spine

    cd /usr/local/src
    apt-get build-dep cacti-spine
    apt install build-essential dos2unix dh-autoreconf help2man libssl-dev libmysql++-dev libmariadb-dev libmariadbclient-dev librrds-perl libsnmp-dev
    git clone https://github.com/Cacti/spine
    cd spine
    git checkout release/1.2.3
    ./bootstrap
    ./configure
    make
    make install

cacti wants to find the path here:

    cd /usr/sbin/
    ln -s /usr/local/spine/bin/spine .

check that spine is in the appropriate and same version as cacti

    spine --version

useful resource for compilation https://www.howtoforge.com/tutorial/install-cacti-on-debian-9/

### Authentication must be Builtin to avoid bugs

Console > Configuration > Settings > Authentication, set the Authentication to "Builtin"

src https://github.com/Cacti/cacti/issues/2369

## interesting utilities found

in `/usr/local/src/cacti/cli` directory there is:

repair_templates.php

upgrade_database.php

# upgrade cacti from older debian to new debian

stop cronjobs, mysqldump and put .sql in the new server as suggested by http://xmodulo.com/migrate-cacti-server.html

in the new machine

    apt-get install cacti cacti-spine

(everything default)

```
mariadb -u root

DROP DATABASE cacti;
CREATE DATABASE cacti;
GRANT ALL PRIVILEGES ON cacti.* TO cacti@localhost IDENTIFIED BY "passwordhere";
FLUSH PRIVILEGES;
```

`passwordhere` must match with auth for cacti: /usr/share/cacti/site/include/config.php

after that: `mysql -u cacti -p cacti < cactidump.sql`
inspired by restore procedure -> src https://www.urban-software.com/cacti-howtos/cacti/backup-guide-for-cacti/

access via web, select "upgrade from"

continue with RDD backup/restore as specified in http://xmodulo.com/migrate-cacti-server.html

extra: http://www.cacti.net/downloads/docs/html/upgrade.html

# official documentation reference

https://www.cacti.net/downloads/docs/html/unix_configure_cacti.html

# extra recommended template for graphs

[download file](extending_interface_graphs_template_196.xml)

src https://forums.cacti.net/viewtopic.php?f=12&t=32345
