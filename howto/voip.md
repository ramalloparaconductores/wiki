# serveis professionals

- empreses properes
  - [capa8](https://capa8.net/en/serveis/telefonia/) intermediari de airenetworks.com
  - [goufone](https://goufone.com/) només els que tenen internet amb ells
  - [som connexió](https://somconnexio.coop) intermediari de masmovil.es
  - [ticae](https://www.ticae.com/) intermediari de airenetworks.com
  - [grn](http://grn.cat/productes/voip/linies-veuip/) pot sortir per 3€ al mes aprox.
- la resta
  - [telsome](https://www.telsome.es/) una persona l'està utilitzant satisfactòriament amb portabilitat
  - [netelip](https://ostel.co/faq) particulars
  - [vozelia](https://www.vozelia.com/es/en/) per empreses?
  - [voztelecom](https://www.voztele.com/) ?
  - sipgate [uk](https://www.sipgatebasic.co.uk/) [de](https://www.sipgate.de/) ?
  - [infocaller](https://infocaller.com/?ch=INFOE) empresas?
  - [polartel](http://polartel.es) empresas?
  - [lista "todos" los operadores voip españa](https://blog.sinologic.net/2014-03/lista-todos-operadores-voip-espana.html)
  - https://tincho.org/VoIP/
  - [nubip](http://www.nubip.com/contacto/]) has de posar-te en contacte amb ells (i preguntar per les tarifes); per particulars m'han dit que OK
- sin pasarela a telefonía tradicional
  - [ostel](https://ostel.co/faq)

extra https://github.com/GuifiBaix/guifibaix-documents/blob/master/preusproveidors/proveidorsvoip.md
