<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [Presentation](#presentation)
- [Install on Debian 9 stretch 2017-10-06](#install-on-debian-9-stretch-2017-10-06)
  - [First steps](#first-steps)
  - [TLS renewal](#tls-renewal)
  - [DNS configuration](#dns-configuration)
  - [Accessibility to the server](#accessibility-to-the-server)
    - [reverse proxy server with nginx](#reverse-proxy-server-with-nginx)
    - [web static client](#web-static-client)
      - [script to upgrade static element](#script-to-upgrade-static-element)
  - [Data](#data)
  - [Test federation](#test-federation)
- [other installation guides](#other-installation-guides)
- [known problems](#known-problems)
  - [notifications](#notifications)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

# Presentation

- [Matrix](https://matrix.org/) server: now [synapse](https://github.com/matrix-org/synapse), comming soon [dendrite](https://github.com/matrix-org/dendrite)
- [How it works?](https://matrix.org/#about)
- [Status of the project](https://matrix.org/blog/2017/07/07/a-call-to-arms-supporting-matrix/). [Extra](https://matrix.org/blog/2017/07/19/status-update/)
- Based on #channels, and @people attached to them. Its access: Guest (Read-only, Read + Write), Login (Local OR LDAP), Encrypted channels through Double [Ratchet algorithm](https://en.wikipedia.org/wiki/Double_Ratchet_Algorithm) - device based encryption (encrypted-devices.png). History control (no, public, semipublic, private)
    - `@user:<server>, #channel:<server>`
    - public channels
        - Who can access this room?
        - Anyone who knows the room's link, including guests
        - List this room <domain>'s room directory?

- Clients:
    - [There are a lot](https://matrix.org/docs/projects/try-matrix-now.html)
    - Recommended: element.io/app HTML5 App, Desktop (Electron), AppStore, Google Play, F-Droid
        - Custom server config (login-element-guifi.png)
        - Room directory (public channels)
        - Start chat
- Bridge with other networks. [Thought](https://xkcd.com/1810/), [response from Matrix](https://twitter.com/matrixdotorg/status/841424770025545730)
- element Integrations: IRC networks (TODO: upload integrations-irc.png), jitsi (TODO: upload jitsi.png), [more](https://medium.com/@RiotChat/riot-im-web-0-12-7c4ea84b180a)
- Locations: Spanish 79%, Catalan 0%

# Install on Debian 9 stretch 2017-10-06

This guide helps you to install a matrix server using authentication of a particular LDAP (guifi.net) with a postgresql database. Hope it helps you to be inspired on your particular needs.

matrix-element homeservers up & running that used this howto:

- https://element.guifi.net
- https://riot.musaik.net

## First steps

If you want to automate this steps (terraform, ansible, docker) you would like to use advice that to debian with the following variable

    export DEBIAN_FRONTEND="noninteractive"

Run all following commands as root user

Add repository

```
cat <<EOF > /etc/apt/sources.list.d/synapse.list
deb http://packages.matrix.org/debian/ stretch main
deb-src http://packages.matrix.org/debian/ stretch main
EOF
```

Add repo key

    curl -L -s https://packages.matrix.org/debian/repo-key.asc | apt-key add -

Install synapse matrix server

    apt-get install matrix-synapse-py3

The two asked options are stored here:

- /etc/matrix-synapse/conf.d/report_stats.yaml
- /etc/matrix-synapse/conf.d/server_name.yaml

Config at `/etc/matrix-synapse/homeserver.yaml` is overridden by config in `/etc/matrix-synapse/conf.d`. Let's add all stuff at `/etc/matrix-synapse/conf.d/guifi.yaml`:

```
cat <<EOF > /etc/matrix-synapse/conf.d/guifi.yaml

 # from synapse version 1 the TLS must be certified (letsencrypt is enough)
tls_certificate_path: "/etc/letsencrypt/live/matrix.example.com/fullchain.pem"
tls_private_key_path: "/etc/letsencrypt/live/matrix.example.com/privkey.pem"

 # overridden: default is sqlite
database:
  name: psycopg2
  args:
    user: synapse_user
    password: synapse_user
    database: synapse
    host: localhost
    cp_min: 5
    cp_max: 10

 # LDAP from guifi

password_providers:
 - module: "ldap_auth_provider.LdapAuthProvider"
   config:
     enabled: true
     uri: "ldaps://ldap.guifi.net"
     start_tls: true
     base: "o=webusers,dc=guifi,dc=net"
     attributes:
        uid: "uid"
        mail: "mail"
        name: "uid"

 # overridden: default is false
allow_guest_access: True

 # reverse proxy -> https://github.com/matrix-org/synapse#using-a-reverse-proxy-with-synapse
 # just adds on port 8008:
 #  + bind_addresses: ['127.0.0.1']
 #  + x_forwarded: true

listeners:
  # Main HTTPS listener
  # For when matrix traffic is sent directly to synapse.
  -
    # The port to listen for HTTPS requests on.
    port: 8448

    # Local interface to listen on.
    # The empty string will cause synapse to listen on all interfaces.
    #bind_address: ''
    # includes IPv6 -> src https://github.com/matrix-org/synapse/issues/1886
    bind_address: '::'

    # This is a 'http' listener, allows us to specify 'resources'.
    type: http

    tls: true

    # Use the X-Forwarded-For (XFF) header as the client IP and not the
    # actual client IP.
    x_forwarded: false

    # List of HTTP resources to serve on this listener.
    resources:
      -
        # List of resources to host on this listener.
        names:
          - client     # The client-server APIs, both v1 and v2
          - webclient  # The bundled webclient.

        # Should synapse compress HTTP responses to clients that support it?
        # This should be disabled if running synapse behind a load balancer
        # that can do automatic compression.
        compress: true

      - names: [federation]  # Federation APIs
        compress: false

  # Unsecure HTTP listener,
  # For when matrix traffic passes through loadbalancer that unwraps TLS.
  - port: 8008
    tls: false
    bind_address: '127.0.0.1'
    type: http

    x_forwarded: true

    resources:
      - names: [client, webclient]
        compress: true
      - names: [federation]
        compress: false

 # enable communities feature
enable_group_creation: True

EOF
```

`/etc/matrix-synapse/conf.d/extra.yaml`:

```
 # The public-facing base URL for the client API (not including _matrix/...)
 #  $ curl -k https://matrix.example.com:8448/_matrix/client/r0/
 # {
 #     "errcode": "M_UNRECOGNIZED",
 #     "error": "Unrecognized request"
 # }
public_baseurl: https://matrix.example.com:8448/
```

[Set up requirements for guifi LDAP](https://github.com/matrix-org/matrix-synapse-ldap3#installation)

    apt-get install python-matrix-synapse-ldap3

[Set up requirements](https://wiki.debian.org/PostgreSql#Installation)

    apt-get install postgresql

create user

    su -s /bin/bash postgres -c "createuser synapse_user"

[enter postresql CLI](https://wiki.debian.org/PostgreSql#User_access):

    su -s /bin/bash postgres -c psql

[put password to user](https://stackoverflow.com/questions/12720967/how-to-change-postgresql-user-password)

    ALTER USER "synapse_user" WITH PASSWORD 'synapse_user';

and [set up database](https://github.com/matrix-org/synapse/blob/master/docs/postgres.rst#set-up-database)

    CREATE DATABASE synapse
     ENCODING 'UTF8'
     LC_COLLATE='C'
     LC_CTYPE='C'
     template=template0
     OWNER synapse_user;


[Set up client in Debian/Ubuntu](https://github.com/matrix-org/synapse/blob/master/docs/postgres.rst#set-up-client-in-debianubuntu)

    apt-get install libpq-dev python-pip python-psycopg2

note: [synapse currently assumes python 2.7 by default](https://github.com/matrix-org/synapse#archlinux)

Start or restart matrix service

    service matrix-synapse restart

To check if is running:

    service matrix-synapse status

## TLS renewal

In guifi.yaml we already added the tls files, extra work is required to access letsencrypt files and to do renewall succesful.

Considering you created a user for the letsencrypt renewal process with letsencrypt user. Add matrix-synapse in the group of letsencrypt

    gpassswd -a matrix-synapse letsencrypt

And change permissions of /etc/letsencrypt directory to be able to access through matrix [reference](https://community.letsencrypt.org/t/how-to-use-certs-in-non-root-services/2690/4)

    chgrp -R letsencrypt /etc/letsencrypt
    chmod -R g=rX /etc/letsencrypt

One time each month a renewal is performed:

```
cat <<EOF > /etc/cron.monthly/matrix-synapse
#!/bin/bash
service matrix-synapse restart
EOF
chmod +x /etc/cron.monthly/matrix-synapse
```

## DNS configuration

This DNS configuration is required to see federation working in your matrix server

[More info setting up federation](https://github.com/matrix-org/synapse#setting-up-federation)

    matrix.example.com IN A <IP>
    element.example.com IN A <IP>
    _matrix._tcp.example.com. 3600 IN SRV 10 0 8448 matrix.example.com.

## Accessibility to the server

Requirements:

    apt-get install certbot nginx-full

### reverse proxy server with nginx

```
matrix_domain="matrix.example.com"
cat <<EOF > /etc/nginx/sites-available/${matrix_domain}
server {
    listen 80;
    listen [::]:80;
    server_name ${matrix_domain};

    location /.well-known {
            default_type "text/plain";
            allow all;
            root /var/www/html;
    }

    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl;
    listen [::]:443 ssl;
    server_name ${matrix_domain};

    ssl_certificate /etc/letsencrypt/live/${matrix_domain}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/${matrix_domain}/privkey.pem;

    # static front page to anounce how works the service
    # example: https://github.com/guifi-exo/public/tree/master/web/matrix.guifi.net
    location / {
        root /var/www/html;
        try_files /matrix.html /matrix.html;
    }

    location /_matrix {
        proxy_pass http://127.0.0.1:8008;
        proxy_set_header X-Forwarded-For $remote_addr;
    }
}
EOF

ln -s /etc/nginx/sites-available/${matrix_domain}.conf /etc/nginx/sites-enabled/${matrix_domain}.conf
certbot certonly -n --keep --agree-tos --email ${matrix_email} --webroot -w /var/www/html/ -d ${matrix_domain}
service nginx reload
```

### web static client

```
element_domain="element.example.com"
element_email="info@example.com"
cat <<EOF > /etc/nginx/sites-available/${element_domain}.conf
server {
    listen 80;
    listen [::]:80;
    server_name ${element_domain};

    location /.well-known {
            default_type "text/plain";
            allow all;
            root /var/www/html;
    }

    return 301 https://\$host\$request_uri;
}

server {
    listen 443 ssl;
    listen [::]:443 ssl;
    server_name ${element_domain};

    ssl_certificate /etc/letsencrypt/live/${element_domain}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/${element_domain}/privkey.pem;

    root /var/www/html/element-web;
}
EOF

ln -s /etc/nginx/sites-available/${element_domain}.conf /etc/nginx/sites-enabled/${element_domain}.conf

certbot certonly -n --keep --agree-tos --email ${element_email} --webroot -w /var/www/html/ -d ${element_domain}

service nginx reload
```

#### script to upgrade static element

Requirements:

    apt-get install jq

take file [element-update.sh](element-update.sh) and put it in `/var/www/html/element-web`

add a config file for your site like `config.element.example.com.json` inside element-web directory `/var/www/html/element-web`

```
{
    "default_server_config": {
        "m.homeserver": {
            "base_url": "https://matrix.example.com",
            "server_name": "matrix.example.com"
        }
    },
    "disable_identity_server": true,
    "disable_custom_urls": true,
    "disable_guests": false,
    "disable_login_language_selector": false,
    "disable_3pid_login": false,
    "brand": "matrix.example.com",
    "integrations_ui_url": "https://scalar.vector.im/",
    "integrations_rest_url": "https://scalar.vector.im/api",
    "integrations_widgets_urls": [
        "https://scalar.vector.im/_matrix/integrations/v1",
        "https://scalar.vector.im/api",
        "https://scalar-staging.vector.im/_matrix/integrations/v1",
        "https://scalar-staging.vector.im/api",
        "https://scalar-staging.riot.im/scalar/api"
    ],
    "integrations_jitsi_widget_url": "https://scalar.vector.im/api/widgets/jitsi.html",
    "bug_report_endpoint_url": "https://element.io/bugreports/submit",
    "defaultCountryCode": "ES",
    "showLabsSettings": false,
    "features": {
        "feature_pinning": "labs",
        "feature_custom_status": "labs",
        "feature_custom_tags": "labs",
        "feature_state_counters": "labs"
    },
    "default_federate": true,
    "default_theme": "light",
    "roomDirectory": {
        "servers": [
            "matrix.example.com"
        ]
    },
    "piwik": {},
    "enable_presence_by_hs_url": {
        "https://matrix.example.com": true
    },
    "settingDefaults": {
        "breadcrumbs": true
    }
}
```

edit file `vi /etc/cron.d/updateelement`, add following content:

```
SHELL=/bin/bash

40 6 * * * root /var/www/html/element-web/bin/element-update.sh
```

new element releases fill directory `/var/www/html/element-web/bundles/`. Check it out from time to time

## Data

Data grows here:

- `/var/lib/postgresql`
- `/var/lib/matrix-synapse/media`
- `/var/log/matrix-synapse/` - warning, uses up to 1 GB, change behavior in `/etc/matrix-synapse/log.yaml`

I symlink this directories to specific volume

learn more about how data grows https://matrix.org/docs/projects/other/hdd-space-calc-for-synapse.html

## Test federation

To test federation you can use this service: https://matrix.org/federationtester/api/report?server_name=matrix.example.com

with more visuals: https://matrix.org/federationtester/ or https://arewereadyyet.com/

source: https://github.com/matrix-org/matrix-federation-tester

# other installation guides

https://matrix.org/docs/guides/installing-synapse

https://matrix.org/docs/guides/#installing-synapse

src https://matrix.org/blog/2019/01/17/bens-favourite-projects-2018/

A walk through of installing Synapse+Riot+Jitsi from scratch on Debian https://matrix.org/blog/2020/04/06/running-your-own-secure-communication-service-with-matrix-and-jitsi

# known problems

## notifications

some people do not receive notifications in its smartphone
