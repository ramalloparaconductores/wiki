Written 2017-09-06

generic reference: https://wiki.openwrt.org/doc/howto/qemu

# Install

```
VMID=9003
RELEASE="19.07.2"
MYFILE="openwrt-${RELEASE}-x86-64-combined-ext4"
MYSTORE="local"
VMNAME="testVM"
qm create ${VMID} --name "${VMNAME}"
qm set "${VMID}" --cpu host --memory 64 --ostype l26 --balloon 0 --serial0 socket
wget "https://downloads.openwrt.org/releases/${RELEASE}/targets/x86/64/${MYFILE}.img.gz"
gunzip "${MYFILE}.img.gz"
qm importdisk ${VMID} "/path/to/${MYFILE}.img" "${MYSTORE}" --format qcow2
qm set "${VMID}" --scsihw virtio-scsi-pci --scsi0 "${MYSTORE}:${VMID}/vm-${VMID}-disk-0.qcow2,cache=writeback"
qm set "${VMID}" --boot c --bootdisk scsi0
```

# Increase root's size

By default it uses 50 MB. You can extend it.

First of all resize VM's disk in proxmox

Put a rescue CD to boot like for example: https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/

Run the operating system as live do `sudo su` and run the following commands:

    parted /dev/sda resizepart 2 100%
    e2fsck -f /dev/sda2
    resize2fs /dev/sda2
    reboot

swap boot from CD to disk, with `df -h` you should see the new desired size
