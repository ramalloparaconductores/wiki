#!/bin/bash
# Copyright (c) 2020 Ramon Selga
# SPDX-License-Identifier: GPL-3.0-or-later

# This script prepares a system with rootfs based on ZFS, UEFI and NVMe disks
# to receive a debian buster tarball

NODE="$1"; shift

# TODO fixme
#IFACE="${IFACE:-eno1}"

#
echo "Setting ZFS root on $NAME with EFI boot from smallest single NVMe disk"
# get EUI for a /dev/nvmeX disk device
get_eui () {
	local disk=$1
        ls -l /dev/disk/by-id/nvme-eui.* | grep -v "part" | grep $disk | cut -d "/" -f 5|cut -d " " -f 1
}
# find first smallest nvme disk
find_boot_drive_id () {
	BOOTDISKID=''
	local disk="$(lsblk -dn -o NAME,SIZE,TYPE,ROTA -x SIZE | grep nvme | awk '{print $1}' | head -n 1)"
        if [ -n "$disk" ]; then
                BOOTDISKID="$(get_eui $disk)"
        fi
}
# wipe existing filessystems in each one partition
wipe_disk () {
	local disk=$1
        for p in $(ls -1 $disk* | grep "part" |sort -r); do
                echo "...wiping partition $p"
                wipefs -a -f $p
        done
        echo "...wiping whole disk $disk"
        wipefs -a -f $disk
}
# create  partitions
create_partitions () {
local disk=$1
echo
echo "Creating GPT partition table..."
sgdisk --zap-all $disk
echo "Creating BIOS boot partition..."
sgdisk -a1 -n1:24K:+1000K -t1:EF02 -c 1:"BIOS boot" $disk
echo "Creating EFI partition."
sgdisk -n2:1M:+512M -t2:EF00 -c 2:"EFI" $disk
echo "Creating Linux boot partition..."
sgdisk -n3:0:+2048M -t3:8300 -c 3:"boot" $disk
echo "Creating ZFS root pool partition ..."
sgdisk -n4:0:0 -t4:BF01 -c 4:"rpool" $disk
echo "Showing partition changes done:"
sgdisk -p  $disk
partprobe
}


# Main

if [ -z "$NODE" ]; then
	echo "NO Node number specified. Exiting"
	exit
fi


NAME="trax$NODE"
IP_NODE="1$NODE"

echo "Setting boot  EFI boot on NVMe for node $NAME"

echo "Finding boot drive: smallest NVMe disk"

find_boot_drive_id

echo

if [ -z "$BOOTDISKID" ]; then
        echo "No boot drive found. Nothing to do."
        exit
fi

BOOTDISK="/dev/disk/by-id/$BOOTDISKID"

# One NVMe disk found
wipe_disk $BOOTDISK
create_partitions $BOOTDISK
partprobe
sleep 2
sync

#exit


# Create boot filesystem

EFI_PART="$BOOTDISK-part2"
BOOT_PART="$BOOTDISK-part3"
ZFS_PART_ID="$BOOTDISKID-part4"

echo "Formatting boot partition with ext4 filesystem ..."
mkfs.ext4 -F $BOOT_PART

# Create zfs root pool rpool

zpool create -o ashift=12 \
        -O acltype=posixacl -O canmount=off -O compression=lz4 \
        -O dnodesize=auto -O normalization=formD -O relatime=on -O xattr=sa \
        -O mountpoint=/ -R /target \
        rpool $ZFS_PART_ID

zpool status

# Create filesystem datasets for root
zfs create -o canmount=off -o mountpoint=none rpool/ROOT
zfs create -o canmount=noauto -o mountpoint=/ rpool/ROOT/pve
zfs mount rpool/ROOT/pve

# Create datasets
zfs create                                 rpool/home
zfs create -o mountpoint=/root             rpool/home/root

# Mount /boot on /target/boot
mkdir /target/boot

mount $BOOT_PART /target/boot

# control point
#exit

# Extract base system root
tar -zvxf /tmp/buster.tar.gz --directory /target
# Create excluded folders
mkdir -p /target/mnt /target/dev /target/sys /target/proc /target/tmp

#
zfs set devices=off rpool

# Set hostname, default is pve.
echo "$NAME" > /target/etc/hostname
echo -e "127.0.1.1\t$NAME" >> /target/etc/hosts

# Format and mount EFI partition /target/boot/efi
mkdir /target/boot/efi
mkdosfs -F 32 -s 1 -n EFI ${EFI_PART}
mount ${EFI_PART} /target/boot/efi

# Set /boot and /boot/efi in fstab
echo -e "UUID=$(blkid -s UUID -o value $BOOT_PART)\t/boot\t\text4\t\tdefaults\t0\t0" > /target/etc/fstab
echo -e "PARTUUID=$(blkid -s PARTUUID -o value ${EFI_PART})\t/boot/efi\tvfat\tnofail,x-systemd.device-timeout=1\t0\t0" >> /target/etc/fstab

# Get network interface name and set target network config
# TODO fixme
#for iface in $(ls -1 /sys/class/net); do
#        if [ -n "$(ip a show dev $iface | grep ' scope global ')" ]; then
#                IFACE=$iface
#                break
#        fi
#done
#echo -e "auto $IFACE\niface $IFACE inet dhcp\n" > /target/etc/network/interfaces.d/$IFACE

# Doing a non-ovs LACP bond in /etc/network/interfaces looks impossible in debian buster
	#   so temp scripts are done to have internet

cat > /target/etc/network/interfaces <<EOF
# interfaces(5) file used by ifup(8) and ifdown(8)
# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

auto eno1
iface eno1 inet manual

auto eno2
iface eno2 inet manual

allow-vmbr0 bond0
iface bond0 inet manual
        ovs_bonds   eno1 eno2
        ovs_type    OVSBond
        ovs_bridge  vmbr0
        ovs_options lacp=active other_config:lacp-time=fast bond_mode=balance-tcp

auto vmbr0
allow-ovs vmbr0
iface vmbr0 inet manual
        ovs_type  OVSBridge
        ovs_ports pve gfs hctrl bond0


allow-vmbr0 pve
iface pve inet static
        address     192.168.96.$IP_NODE/24
        ovs_type    OVSIntPort
        ovs_bridge  vmbr0
        ovs_options tag=96

allow-vmbr0 gfs
iface gfs inet static
        address     192.168.97.$IP_NODE/24
        ovs_type    OVSIntPort
        ovs_bridge  vmbr0
        ovs_options tag=97

allow-vmbr0 hctrl
iface hctrl inet static
        address     192.168.98.$IP_NODE/24
        gateway     192.168.98.29
        ovs_type    OVSIntPort
        ovs_bridge  vmbr0
        ovs_options tag=98

EOF

cat > /target/etc/resolv.conf <<EOF
domain exo.cat
search exo.cat
nameserver 1.1.1.1
nameserver 8.8.4.4
EOF

# Set grub default root

sed -i 's#GRUB_CMDLINE_LINUX=""#GRUB_CMDLINE_LINUX="root=ZFS=rpool/ROOT/pve"#' /target/etc/default/grub
sed -i 's#_LINUX_DEFAULT="quiet"#_LINUX_DEFAULT=""#' /target/etc/default/grub


# Prepare script for chroot
echo "Preparing chroot"
cat > /target/root/chroot-tasks.sh <<EOF
#!/bin/bash
DISK=$BOOTDISK
grub-probe /boot
update-initramfs -u -k all
update-grub
apt install --yes grub-efi-amd64 openvswitch-switch
mount -t efivarfs efivarfs /sys/firmware/efi/efivars
grub-install --target=x86_64-efi --efi-directory=/boot/efi \
    --bootloader-id=pve --recheck --no-floppy
umount /sys/firmware/efi/efivars
ls /boot/grub/*/zfs.mod
zfs snapshot rpool/ROOT/pve@install
echo "Exiting from chroot"
cp /root/.bashrc-bak /root/.bashrc
exit
EOF
chmod +x /target/root/chroot-tasks.sh

# inject chroot tasks as .bashrc login -> src https://askubuntu.com/questions/551195/scripting-chroot-how-to/551200#551200
cp /target/root/.bashrc /target/root/.bashrc-bak
cp /target/root/chroot-tasks.sh /target/root/.bashrc

#Prepare mounts and jump to chroot
echo "Jump to chroot"
mount -o bind /dev /target/dev
mount -o bind /sys /target/sys
mount -o bind /proc /target/proc
chroot /target /bin/bash --login

mount | grep -v zfs | tac | awk '/\/target/ {print $3}' | \
    xargs -i{} umount -lf {}
sleep 2
zpool export -a
echo "Setup done!"
