work in progress !

<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [script to generate debian image](#script-to-generate-debian-image)
- [script to generate template](#script-to-generate-template)
  - [provision a VM](#provision-a-vm)
  - [cloudinit changes when VM is already created](#cloudinit-changes-when-vm-is-already-created)
  - [extra](#extra)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->
cloudinit is a way to facilitate the provision of virtual machines in the proxmox environment

# script to generate debian image

use debian buster and install requeriment `apt install openstack-debian-images`, the following script is an adaptation of [build-openstack-debian-image](./build-openstack-debian-image) but that works in xfs: [build-openstack-debian-image-xfs](./build-openstack-debian-image-xfs)

    ./build-openstack-debian-image-xfs -r buster

# script to generate template

here it is a script to create a VM template ready for cloudinit usage

see script [create_template_cloudinit.sh](./create_template_cloudinit.sh)

## provision a VM

script way:

see script [create_VM_cloudinit.sh](./create_VM_cloudinit.sh)

or you can do it manually

## cloudinit changes when VM is already created

only the two core variables (ip and ssh keys) were inspected

- **ip config**: cloudinit's network is modified in complementary file `/etc/network/interfaces.d/50-cloud-init.cfg`, there it says a way to disable it

```
# This file is generated from information provided by
# the datasource.  Changes to it will not persist across an instance.
# To disable cloud-init's network configuration capabilities, write a file
# /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg with the following:
# network: {config: disabled}

# here follows your cloudinit's particular config
```

- **ssh hosts**: changes in the ssh public key list are append only: a key is never deleted, only new ones are added

## extra

- does not work, but is interesting https://stackoverflow.com/questions/23065673/how-to-re-run-cloud-init-without-reboot/50911376#50911376
