You may need to downgrade the original AirOS firmware to v5.5 in order to allow a firmware upload via the web interface. Upload via TFTP should work even for new AirOS versions. src https://openwrt.org/toh/ubiquiti/nanostationm5

| product | link | sha256sum | comments |
| ------- | --------- | --------- | -------- |
| XM series | [link](XM.v5.5.11.28002.150723.1344.bin) | bfc85c68b16c760b112f763ade4e6c3c4ac49c7e1f46436fd67ec7b911719600 | tested |
| XW series | [link](XW.v5.5.10-u2.28005.150723.1358.bin) | b222ae2fccd3a108cfc170197ca9b588048edf137846891b71ec863995bb7954 | tested / [official link](https://dl.ubnt.com/firmwares/XW-fw/v5.5.10/XW.v5.5.10-u2.28005.150723.1358.bin) |
| ? | [link](TI.v5.5.11.28002.150723.1518.bin) | 201e024b081d6b828822ada56c4282d40bb8ff1b9489442ff94c2ece046129c4 | ? |
| AC Lite | [link](UniFi-firmware-3.7.58-for-UAP-AC-Lite-LR-Pro-EDU-M-M-PRO-IW-IW-Pro__BZ.qca956x.v3.7.58.6385.170508.0957__2017-05-15.bin) | cfe2e639c430d1a849c2892effb84157e82ea9cb08d174547c1e001d4d4388a8 | we are going to test it. Modified original filename (substring between `__`). More info https://openwrt.org/toh/ubiquiti/unifiac |
| WA series | [link](WA.v8.4.1.35794.171006.1417.bin) | 15211e388b86839ff1f1199d44e1e86d0cf07a4725ca3e68de6fab3eb746813f | tested, follow procedure below |


Other references about downgrade:

-  Downgrade to XW AirOS 5.6.15 in the Web interface. Then to XW AirOS 5.5.10 using TFTP recover mode and then flash OpenWrt Firmware using TFTP. src https://openwrt.org/toh/ubiquiti/powerbeam?s[]=downgrade
    - [XW.v5.6.15-sign.31612.170908.1440.bin](XW.v5.6.15-sign.31612.170908.1440.bin) or [official link](https://dl.ubnt.com/firmwares/XW-fw/v5.6.15/XW.v5.6.15-sign.31612.170908.1440.bin) sha256sum 3b54cf10d5056614362be0644bf0296ea61647c0169cf0805065e1371b0af6e6
- Potentially useful beta firmwares https://community.ubnt.com/t5/airMAX-Installation/AirOS-v6-0-6-Firmware-Issues-and-Solutions-Downgrade-Path-and-XM/td-p/1996704
- from source https://openwrt.org/toh/ubiquiti/nanostationm2?s[]=downgrade
    - for AirOS 6.0.x | using fwupdate src https://technicalexperiments.wordpress.com/2017/08/10/downgrading-ubnt-nanostation-m2-loco-xm-ubnt-downgrade-code2-msgfirmware-check-failed/
    - for AirOS 6.x.x | https://forum.openwrt.org/t/install-openwrt-on-ubiquiti-nanostation-m2-xw-signed/36371
- use mtd https://github.com/true-systems/ubnt-openwrt-flashing#flashing-sysupgrade-image-using-mtd-over-ssh-in-airos-v617 found here https://forum.openwrt.org/t/new-ubiquiti-loco-m2-xw/5760/44
- if none of the other items work get more wood here: https://openwrt.org/start?do=search&id=start&q=downgrade

# extra: llorenç procedure

From AirOS to Openwrt

```
XW.v5.6.15-sign.31612.170908.1440.bin via tftp ->
XW.v6.1.9.32918.190108.1737.bin via web ->
XW.v6.1.4.32113.180112.0918.bin via web ->
XW.v6.1.2.31825.171017.1633.bin via web ->
XW.v6.0.6-beta.30875.170526.0023.bin via web ->
XW.v5.6.15.30572.170328.1052.bin via web ->
XW.v5.5.10-u2.28005.150723.1358.bin via web ->
openwrt via web!!
```

check images here:

http://mortitx.pc.ac.upc.edu/~llorenc/ubnt/

once this process is optimized we will include all images

starting in `XW.v6.0.6-beta.30875.170526.0023.bin` works

# WA (Airmax AC) AirOS ~8

applies to devices:

- nanostation ac
- [liteAP](https://openwrt.org/toh/hwdata/ubiquiti/ubiquiti_liteap_ac_lap-120)

[ this is backup of this part src https://openwrt.org/inbox/ubiquiti/ubiquiti_nanostation_ac#installing_openwrt ]

The firmware for this ubiquiti device is signed. But since there are no mtd utilities available on the device the original fwupdate.real binary must be used. Please make sure you have got firmware version v8.4.1.35794 installed https://dl.ubnt.com/firmwares/XC-fw/v8.4.1/WA.v8.4.1.35794.171006.1417.bin (check table above for a backup link). The instructions probably won't work for other versions. To install unsigned firmware the fwupdate.real binary (which is in fact just a symlink to ubntbox) must be patched:

    hexdump -Cv /bin/ubntbox | sed 's/14 40 fe ff/00 00 00 00/g' | hexdump -R > /tmp/fwupdate.real
    chmod +x /tmp/fwupdate.real

After that the factory image can be installed using the patched binary:

    /tmp/fwupdate.real -m /tmp/openwrt-ath79-generic-ubnt-nanostation-ac-squashfs-factory.bin
