Replacing "massmedia" ISPs routers

Info for Spain / Catalonia

challenge: neutral router that can communicate with GPON technology directly

right now we only can "neutralize" router when it can behave just as a [ONT](https://en.wikipedia.org/wiki/Passive_optical_network#Elements_and_Characteristics_of_a_PON). If you don't have a separated ONT you need IDONT to introduce it in another device you are going to replace; if is a new installation ask to the technician.

- **movistar**: vlan 6 with generic PPPoE authentication. user: `adslppp@telefonicanetpa` password: adslppp (tested)
    - in case you have [Askey HGU RFT3505VW](http://www.movistar.es/particulares/atencion-cliente/internet/adsl/equipamiento-adsl/routers/#ftth) to obtain IDONT: http://192.168.1.1/instalacion src https://comunidad.movistar.es/t5/Soporte-Fibra-y-ADSL/HDG-ASKEY-INTRODUCIR-IDONT/td-p/2916272
- **jazztel**: vlan 1074 with a simple DHCP, sometimes is tricky to get access and you have to wait and/or reboot devices at the same/different time (tested)
- **vodafone**: vlan 100 with custom PPPoE (tested. requires sniffing TR-069 traffic)
    - details step by step, requires separation between router and ont: https://www.redeszone.net/2017/01/21/manual-para-configurar-vodafone-ftth-con-el-sistema-operativo-pfsense-actuando-de-router-neutro/
    - https://bandaancha.eu/articulos/conseguir-admin-router-sercomm-h500-s-9602 but password is VF-ESVodafone-H-500-s
- **orange**:
  - one case was vlan 1074 with a simple DHCP (tested). The *Livebox Fibra* you receive can be configured as an ONT (so you can manage the IP address with your own neutral router). Alternatively, in the WebGUI of *Livebox Fibra* they give the ONT password you can introduce in the neutral ONT
    - extra src https://www.adslzone.net/foro/jazztel.10/livebox-fibra-como-ont-imposible.451362/#post-3133746
  - another case was vlan 832 with a simple DHCP (tested). ONT separated from Livebox Fibra. Serve DHCP server in vlan 832 to the wan port of the livebox wifi-router-voip to get voip working. You need to do port forwarding of 5060 port with source addresses of at least hosts in proxy.sip.orange.es and proxy2.sip.orange.es; maybe you want to accept the whole 85.62.0.0/16
- **masmobil** vlan 20 with a simple DHCP (tested)
  - **pepephone**: They use ZTE F680 as integrated device (ont + router-wifi-voip), this router is not working in bridge mode (then, use an alternative ont that is huawei or zte). To access administrator: login as admin/admin; If you already connected to Internet press reset button for 30 seconds and login as admin/admin.
  - **somconnexio**: I my case I could opt between two separate devices (alcatel-lucent ont and zte router-wifi-voip) or one that integrates all (zte), I opted for two devices as I want a trusted device in the edge, and that requires an ont in bridge mode. This ONT is G-010G-P from Alcatel-Lucent with IP 192.168.100.1 and access admin/1234, installer gives ont password. wifi-router-voip device is 192.168.1.1 1234/1234, its data is autoprovisioned so it is still TODO to get voip or have this device in second term with a dhcp server listening for their wan vlan 20 and do the required port forwarding for voip

src https://wiki.bandaancha.st/Identificadores_VLAN_operadores_FTTH

src https://naseros.com/2017/02/01/configuracion-de-un-router-neutro-y-configuracion-de-las-vlan/

request: generic procedure for port mirroring
    - (not tested) https://github.com/mmaraya/port-mirroring
    - (not tested) http://www.persianov.net/tutorials/how-to-setup-openwrt-traffic-mirroring-and-snort-ids/

# ONT to use

compatible providers with all: huawei, zte

## ONT to buy

- Ubiquiti Ufiber Nano. [in landashop](https://www.landashop.com/ubn-uf-nano.html)
- Huawei echolife hg8010h (recommended for movistar neba). [one provider](https://cdr.pl/p5169,huawei-echolife-hg8010h-ont-1x-ge-1x-gpon.html)

## ONT to reuse

- ONT ZTE F601 (used by jazztel in the early FTTH days).
    - guide (spanish): https://www.youtube.com/playlist?list=PLSvxAUzJ-XSfhk07w737c5p403a6U_WPc
- huawei hg8240 / huawei hg8240h (used by movistar)
    - [how to access via web](http://movistarencuestafibra.blogspot.com/p/ont-hg.html) - telecomadmin/admintelecom 192.168.100.1).
    - telnet access u: root p: admin
    - decode config file (linux and windows) https://zedt.eu/tech/hardware/obtaining-administrator-access-huawei-hg8247h/
    - [method to get current data](https://wiki.bandaancha.st/Obtener_datos_Router_ZTE_F680)
    - known problem: when you plug optical fiber you loose access, so you cannot reboot the device remotely. That's not good!
