# nginx configuration guides

<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [general notes](#general-notes)
- [nginx as reverse proxy](#nginx-as-reverse-proxy)
    - [nginx.conf](#nginxconf)
    - [site template](#site-template)
- [add letsencrypt to the domain](#add-letsencrypt-to-the-domain)
    - [install certbot](#install-certbot)
    - [prepare letsencrypt certificate](#prepare-letsencrypt-certificate)
    - [procedure](#procedure)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

## general notes

when you change configuration in nginx first of all **test your configuration** to avoid downtime

    nginx -t

then reload it

    service nginx reload

sometimes to apply configuration you require to restart it

    service nginx restart

if there are troubles check log

    tailf /var/log/nginx/error.log

## nginx as reverse proxy

### nginx.conf

in `/etc/nginx.conf` remove lines in section `SSL Settings` and put the following

        ##
        # SSL Settings
        ##

        #ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
        ssl_protocols TLSv1.2; # Dropping SSLv3, ref: POODLE
        ssl_prefer_server_ciphers on;

        # more ssl settings -> src https://cipherli.st/
        ssl_dhparam /etc/nginx/dhparam.pem; # openssl dhparam -out /etc/nginx/dhparam.pem 4096
        ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384;
        ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
        ssl_session_timeout  10m;
        # enable session resumption to improve https performance
        # http://vincent.bernat.im/en/blog/2011-ssl-session-reuse-rfc5077.html
        ssl_session_cache shared:SSL:10m;
        ssl_session_tickets off; # Requires nginx >= 1.5.9
        ssl_stapling on; # Requires nginx >= 1.3.7
        ssl_stapling_verify on; # Requires nginx => 1.3.7
        # enable if you have own dns and cache
        #resolver $DNS-IP-1 $DNS-IP-2 valid=300s;
        resolver_timeout 5s;
        add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
        ## nextcloud says: The "X-Frame-Options" HTTP header is not set to "SAMEORIGIN". This is a potential security or privacy risk, as it is recommended to adjust this setting accordingly.
        #add_header X-Frame-Options DENY;
        ## nextcloud says: The "X-Content-Type-Options" HTTP header is not set to "nosniff". This is a potential security or privacy risk, as it is recommended to adjust this setting accordingly.
        #add_header X-Content-Type-Options nosniff;
        add_header X-XSS-Protection "1; mode=block";

        # https://securityheaders.com gives me warning but don't know how to fix it -> src https://fearby.com/article/set-up-feature-policy-referrer-policy-and-content-security-policy-headers-in-nginx/
        add_header 'Feature-Policy' "geolocation: 'none';midi: none;notifications: none;push: none;sync-xhr: none;microphone: none;camera: none;magnetometer: none;gyroscope: none;speaker: self;vibrate: none;fullscreen: self;payment: none;";
        add_header Feature-Policy "geolocation none;midi none;notifications none;push none;sync-xhr none;microphone none;camera none;magnetometer none;gyroscope none;speaker self;vibrate none;fullscreen self;payment none;";

        # The "Referrer-Policy" HTTP header is not set to "no-referrer", "no-referrer-when-downgrade", "strict-origin" or "strict-origin-when-cross-origin". This can leak referer information. See the W3C Recommendation ->
        #  https://www.w3.org/TR/referrer-policy/#referrer-policy-no-referrer
        #  https://stackoverflow.com/questions/43447490/how-to-set-referrer-policy-with-nginx
        ## if uncommented nextcloud in reverse proxy mode says: The "Referrer-Policy" HTTP header is not set to "no-referrer", "no-referrer-when-downgrade", "strict-origin", "strict-origin-when-cross-origin" or "same-origin". This can leak referer information. See the W3C Recommendation ↗.
        #add_header Referrer-Policy 'no-referrer';

        # src https://scotthelme.co.uk/content-security-policy-an-introduction/
        #add_header 'Content-Security-Policy' "script-src 'self";


In `Gzip Settings` disable gzip itself:

```
        ##
        # Gzip Settings
        ##

        # Note: You should disable gzip for SSL traffic.
        # See: https://bugs.debian.org/773332
        # src https://serverfault.com/questions/544279/nginx-disable-gzip-compression-for-https-only
        gzip off;

        # extra src -> https://gist.github.com/plentz/6737338

        #gzip on;

```

### site template

this could serve as a template. In `/etc/nginx/sites-available/example.com`

```
server {
        listen 80;
        listen [::]:80;
        server_name example.com;

        location /.well-known {
                default_type "text/plain";
                allow all;
                root /var/www/html;
        }

        location / {
                return 301 https://$host$request_uri;
        }
}

server {
        listen 443 ssl;
        listen [::]:443 ssl;
        server_name example.com;

        ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem;

        location / {
                proxy_pass http://192.168.95.24;
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                # pero sigue sin funcionar bien rt (CSRF a veces) -> https://github.com/osTicket/osTicket/issues/736#issuecomment-71351921
                proxy_pass_header Set-Cookie;
        }
}
```

then do a symlink to enabled site

    cd /etc/nginx/sites-enabled/
    ln -s ../example.com

## add letsencrypt to the domain

### install certbot

You need stretch-backports. Add line in `/etc/apt/sources.list.d/backports.list`

    deb http://http.debian.net/debian stretch-backports main contrib

install certbot

    apt install -t stretch-backports certbot

create all the things necessary to run a letsencrypt user with proper permissions

    adduser letsencrypt
    chown letsencrypt:letsencrypt /home/letsencrypt/
    mkdir /etc/letsencrypt
    chown letsencrypt:letsencrypt /etc/letsencrypt
    mkdir /var/lib/letsencrypt
    mkdir /var/log/letsencrypt
    chown letsencrypt:letsencrypt /var/log/letsencrypt/
    mkdir /var/www/html/.well-known
    chown letsencrypt:letsencrypt -R /var/www/html/.well-known

doing that means we have to prevent systemd cronjobs

```
systemctl stop certbot.timer
systemctl disable certbot.timer
systemctl stop certbot.service
systemctl disable certbot.service
```

and in file `/etc/cron.d/certbot` adjust our certbot cronjob to use our user and not root

```
0 */12 * * * letsencrypt test -x /usr/bin/certbot && perl -e 'sleep int(rand(3600))' && certbot -q renew &> /dev/null
```

take care too during upgrades, debian will warn that this files wants to be changed, in general you can say "don't change it"

### prepare letsencrypt certificate

first step: point DNS accordingly

this is only the first part of the site template (because the SSL part is not ready)

```
server {
        listen 80;
        listen [::]:80;
        server_name example.com;

        location /.well-known {
                default_type "text/plain";
                allow all;
                root /var/www/html;
        }

        location / {
                return 301 https://$host$request_uri;
        }
}
```

### procedure

run the command as letsencrypt user

    su letsencrypt

execute the command (to a domain or subdomain, as you need)

    certbot certonly -n --keep --agree-tos --email info@example.com --webroot -w /var/www/html/ -d subdomain.example.com

