<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [Configuració de túnels L2TP per routers OpenWRT/LEDE](#configuració-de-túnels-l2tp-per-routers-openwrtlede)
  - [Instal·lació dels paquets necessaris](#instal·lació-dels-paquets-necessaris)
  - [Configuració de la connexió a Guifi.net](#configuració-de-la-connexió-a-guifinet)
  - [Configuració de la connexió L2TP d'accés a Internet IPv4](#configuració-de-la-connexió-l2tp-daccés-a-internet-ipv4)
  - [Configuració de la connexió d'accés a Internet per IPv6](#configuració-de-la-connexió-daccés-a-internet-per-ipv6)
  - [Activació de la reconnexió automàtica](#activació-de-la-reconnexió-automàtica)
- [Configuració de túnels L2TP per routers Mikrotik (RouterOS)](#configuració-de-túnels-l2tp-per-routers-mikrotik-routeros)
  - [Configuració de la connexió d'accés a Internet per IPv6](#configuració-de-la-connexió-daccés-a-internet-per-ipv6-1)
- [Configuració de túnels L2TP per debian i derivats](#configuració-de-túnels-l2tp-per-debian-i-derivats)
  - [instruccions extra VyOS o EdgeOS](#instruccions-extra-vyos-o-edgeos)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

# Configuració de túnels L2TP per routers OpenWRT/LEDE

Autor: Víctor Oncins 2017-11-12
Revisió: Roger Pueyo Centelles 2020-05-29

El servei d'accés a Internet a través de l'eXO amb el protocol L2TP presenta certs avantatges. D'una banda millora els processos de gestió i control de la subscripció a Internet. De l'altra, simplifica la configuració dels routers residencials i permet integrar el suport de IPv4 i IPv6 a través d'una sola connexió. Els routers que suporten el sistema OpenWRT/LEDE permeten aquest tipus de connexió. Tanmateix hi ha routers amb firmwares propietaris que també el suporten. Si teniu un router residencial amb el firmware original, comproveu que el protocol L2TP està suportat.

El protocol L2TP encapsula una connexió de capa d'enllaç de tipus PPP. Per obtenir més informació cliqueu el document de referència [LAA](https://github.com/guifi-exo/wiki/blob/master/howto/arquitectura-laa-acces-inet.md). La connexió PPP requereix un nom d'usuari i un password que serà proveït per l'associació eXO. Un cop disposem d'aquestes dades ja podem configurar el router OpenWRT. Els esquemes d'autenticació suportats pel concentrador són CHAP i MSCHAP.

Aquesta és una guia tècnica per configurar aquest servei en routers residencials amb OpenWRT/LEDE. **Aquest document cobreix les versions estables d'OpenWrt 15.05.1 (Chaos Calmer), LEDE 17.01.4 (Reboot), OpenWrt 18.06.1 i OpenWrt 19.07.3.**

Podem trobar les imatges pre-compilades al dipòsit del projecte [OpenWRT](https://downloads.openwrt.org/chaos_calmer/15.05.1/), [LEDE](https://downloads.lede-project.org/releases/). Trieu l'arquitectura de hardware i el model de router i procediu a gravar el nou firmware seguint les recomanacions dels webs dels respectius projectes. **Cal tenir en compte que no es recomana fer ús de dispositius de 4M de flash i 32M de RAM o menys.**

## Instal·lació dels paquets necessaris

Suposem que tenim un router amb un firmware OpenWRT amb les configuracions per defecte. Un cop el tinguem a punt, connecteu el port WAN a una connexió a Internet. Si els valors de la configuració són els per defecte, hi haurà un client DHCP activat. Entrem al web de configuració http://192.168.1.1/. Entrem a la part *System > Software*. Cliquem *Update lists* i actualitzem la llista de dipòsits.

Cerquem el paquet *xl2tpd* i l'instal·lem. De manera opcional podem instal·lar d'altres com ara el `ip` o el `tcpdump` que poden ser útils en una fase posterior de depuració de problemes. Si feu servir LEDE el paquet *ip* ja ve instal·lat. Alternativament podem instal·lar tots els paquets de cop per línia de comandes:

```
root@OpenWrt:~# opkg update
root@OpenWrt:~# opkg install xl2tpd ip tcpdump-mini
```

Per configurar la connexió a través de LuCI, la interfície web del router, cal instal·lar adicionalment el paquet `luci-proto-ppp`:

```
root@OpenWrt:~# opkg install luci-proto-ppp
```

## Configuració de la connexió a Guifi.net

Cal tenir clar quina és la IP i rang (màscara) del node comunitari, típicament situat al terrat. Consulteu la pàgina web de Guifi.net o bé accediu directament al node. Normalment la IP té el format `10.a.b.c/27`. Reservem una nova IP dins d'aquest rang per router residencial, per exemple la `10.a.b.c+1/27`.

Anem a la part *Interfaces* i editem la WAN. Triem en el desplegable *Static address* i cliquem *Switch protocol*. Emplenem els camps amb aquests valors:

```
IPv4 address = 10.a.b.c+1
IPv4 netmask = 255.255.255.224
IPv4 gateway = <buit>
```
Per tal d'assegurar la connectivitat tant amb Internet com amb guifi, afegim una ruta estàtica a *Network* i *Static routes*:

```
Interface = wan
Target = 10.0.0.0/8
IPv4 Gateway = 10.a.b.c
```

A l'apartat *Firewall Settings*, vinculem la interfície de xarxa a la zona WAN del firewall. Això impedirà l'accés de les connexions entrants al router residencial. Apliquem i desem canvis. Connectem el port que correspongui a la interfície WAN al cable del node comunitari o a l'equipment de xarxa que hi permeti l'accés. Assegureu-vos que teniu accés al concentrador de túnels de l'eXO:

```
root@OpenWrt:~# ping 10.38.140.225
PING 10.38.140.225 (10.38.140.225): 56 data bytes
64 bytes from 10.38.140.225: seq=0 ttl=61 time=6.780 ms
64 bytes from 10.38.140.225: seq=1 ttl=61 time=4.980 ms
^C
--- 10.38.140.225 ping statistics ---
2 packets transmitted, 2 packets received, 0% packet loss
round-trip min/avg/max = 4.980/5.880/6.780 ms
```

## Configuració de la connexió L2TP d'accés a Internet IPv4

Ens situem a la part de *Interfaces* i afegim una nova interfície clicant el botó *Add interface*. L'anomenem `exo`. Triem com a protocol de la nova interfície el L2TP. No hi vinculem cap interfície física. Apliquem els canvis i apareixeran els camps que caldrà emplenar:

```
L2TP Server = 10.38.140.225
CHAP username = <nom d'usuari proveït per eXO>
CHAP password = <password proveït per eXO>
```
A la secció *Advanced Settings*, marquem els punts següents:

- [X] Bring up on boot
- [x] Use builtin IPv6-management
- [ ] Trieu l'opció *Manual* del desplegable si feu servir LEDE
- [x] Use default gateway
- [x] Use DNS servers advertised by peer (Opcional)

**Cal que `Override MTU = 1420`.** Situem la nova interfície a la zona WAN del firewall a la part *Firewall Settings*. Apliquem i desem canvis.

## Configuració de la connexió d'accés a Internet per IPv6

L'eXO també proveeix d'accés a Internet per IPv6. En primer lloc caldrà suprimir el prefixe [ULA](https://en.wikipedia.org/wiki/Unique_local_address) que ve per defecte. Deixem en blanc el camp *IPv6 ULA-Prefix* a la part inferior de *Interfaces*. Per activar IPv6 global caldrà editar la interfície WAN6. Anem a *Physical Settings* i activem la següent opció tot emplenant el camp de text amb el valor `@exo`

- [X] Custom interface [ @exo ]

Apliqueu i deseu canvis. Per tal de fer efectiva la delegació del prefix IPv6 provinent del concentrador, caldrà editar la interfície LAN i modificar el camp següent:

```
IPv6 assigment length = 64
```

Deseu, apliqueu canvis i reinicieu el router. Un cop recuperat, ja hauríeu de poder accedir a Internet per IPv4 i IPv6.

pels usuaris avançats, podeu aplicar lo mateix per línia de comandes de la següent manera (però exo6 enlloc de wan6, com vulgueu), no copieu i enganxeu, enteneu línia a línia que està passant i modifiqueu com calgui en el vostre router en particular:

```
uci delete network.globals.ula_prefix
uci set network.lan.ip6assign='64'
uci set firewall.@zone[2].network='wan wan6 exo exo6'
uci set network.exo6=interface
uci set network.exo6.ifname='@exo'
uci set network.exo6.proto='dhcpv6'
uci set network.exo6.reqaddress='try'
uci set network.exo6.reqprefix='auto'
uci commit
```

## Activació de la reconnexió automàtica

Per defecte la reconnexió del túnel en cas d'interrupció de la comunicació està deshabilitada. Per activar-la caldrà editar l'arxiu `/etc/config/network` i afegir el camp `checkup_interval` amb el valor en segons del temps de reintent de connexió. Podem afegir també el camp `keepalive` per modular les comprovacions periòdiques de l'estat del túnel. En el nostre exemple, cada 10 segons enviarà un paquet de keepalive. Si durant 20 segons no es rep cap altre keepalive el túnel es considera desconnectat. Tornarà a fer intents cada 10 segons per tornar-lo a aixecar.

```
config interface 'exo'
        option proto 'l2tp'
        option server '10.38.140.225'
        option username '<el-vostre-usuari>'
        option password '<el-vostre-password>'
        option ipv6 '1'
        option mtu <el-vostre-mtu>
        option checkup_interval '10'
        option keepalive '20,10'
```

**Important**: tal com s'ha verificat, `option ipv6 'auto'` no farà una assignació correcte de IPv6 via el L2TP, cal que sigui `option ipv6 '1'`

Un cop desats els canvis, reinicieu el router i ja tindrem la funció de reestabliment habilitada i la resta de configuracions aplicades.

# Configuració de túnels L2TP per routers Mikrotik (RouterOS)

No ens consta una versió mínima de RouterOS però hauria de funcionar amb qualsevol. Les proves les hem fet amb 6.36 en endavant. Considerem una configuració prèvia habitual:

```
/ip address
add address=192.168.0.1/24 interface=lan
add address=10.a.b.c+1/27 interface=guifi

/ip firewall nat
add action=masquerade chain=srcnat out-interface=guifi

/ip route
add distance=1 dst-address=10.0.0.0/8 gateway=10.a.b.c

/system ntp client
set enabled=yes primary-ntp=10.228.203.104

```
Assegureu-vos que teniu accés al concentrador de túnels de l'eXO:

```
[admin@MyGuifiNode] > ping 10.38.140.225
  SEQ HOST                                     SIZE TTL TIME
    0 10.38.140.225                              56  58 30ms
    1 10.38.140.225                              56  58 31ms
    2 10.38.140.225                              56  58 36ms
    sent=3 received=3 packet-loss=0% min-rtt=30ms avg-rtt=32ms max-rtt=36ms
```
Afegim ara la interfície L2TP:

```
/interface l2tp-client
add add-default-route=yes connect-to=10.38.140.225 disabled=no keepalive-timeout=20 max-mru=<el-nostre-mru> max-mtu=<el-nostre-mtu> name=exo password=<el-meu-password> user=<el-meu-user>
```
**Cal que `max-mtu = 1420 max-mru = 1420`.** A partir de la versió 6.39 ja no es creen automàticament regles de mangle per l'ajustament de MSS. En aquest cas, l'ajustament es fa de manera implícita des del perfil PPP vincular a la connexió L2TP.
Ara afegim la regla de SNAT sobre la nova interfície *exo*:
```
/ip firewall nat
add action=masquerade chain=srcnat out-interface=exo
```
Si la interfície *exo* està *up*, en aquest moment ja hauríem de tenir accés a Internet. RouterOS no té un firewall tant predefinit com OpenWRT i per tant, podem personalitzar-lo més o menys. Proposem això:
```
/ip firewall filter
add action=accept chain=input protocol=icmp
add action=accept chain=input in-interface=guifi
add action=accept chain=input in-interface=lan
add action=accept chain=input connection-state=established,related
add action=drop chain=input
```
## Configuració de la connexió d'accés a Internet per IPv6
En primer lloc cal comprobar que el paquet IPv6 del Router està habilitat:
```
[admin@MyGuifiNode] > system package print
Flags: X - disabled
 #   NAME                                                                      VERSION
 0   routeros-mipsbe                                                           6.42.6
 1   system                                                                    6.42.6
 2   ipv6                                                                      6.42.6
 3   wireless                                                                  6.42.6
 4   hotspot                                                                   6.42.6
 5   dhcp                                                                      6.42.6
 6   mpls                                                                      6.42.6
 7   routing                                                                   6.42.6
 8   ppp                                                                       6.42.6
 9   security                                                                  6.42.6
10   advanced-tools                                                            6.42.6
```
Si observem una *X* al costat del paquet, caldrà habilitar-lo i reiniciar el router:
```
[admin@MyGuifiNode] > system package enable ipv6
```
Per defecte el perfil PPP que ve amb la interfície L2TP permet l'ús de IPv6. Assegurem-nos que és així. El servidor d'accés lliurarà un prefixe /60 IPv6 de delegació. Per demanar-lo caldrà configurar:

```
/ipv6 dhcp-client
add interface=exo pool-name=exo-prefix pool-prefix-length=60 request=prefix use-peer-dns=no
```
El RouterOS automàticament crearà un pool d'adreces que seran assignades a les interfícies delegades. Per declarar un interfície delegada:
```
/ipv6 address
add from-pool=exo-prefix interface=lan
```
A partir d'aquest moment ja hauríem de tenir accés a Internet per IPv6. Caldrà protegir minimament els dispositius connectats amb el firewal IPv6:
```
/ipv6 firewall filter
add action=accept chain=input protocol=icmpv6
add action=accept chain=input dst-port=546 in-interface=exo protocol=udp
add action=accept chain=input connection-state=established,related
add action=drop chain=input
add action=accept chain=forward connection-state=established,related
add action=drop chain=forward connection-state=!established,related in-interface=exo
```

# Configuració de túnels L2TP per debian i derivats

Notes preliminars:

- les accions necessàries per edgemax (i probablement per vyos) es comenten al final
- provat amb mikrotik l2tp server i accelppp

Si és un debian probablement no estarà instal·lat el paquet (en VyOS y EdgeMAX ve instal·lat per defecte):

    apt install xl2tpd

Guardar un backup de la configuració exemple

    mv /etc/xl2tpd/xl2tpd.conf /etc/xl2tpd/xl2tpd.conf.bak

En el fitxer `/etc/xl2tpd/xl2tpd.conf` hi haurà el contingut:

```
; general note: see debug output in /var/log/syslog

[global]
port = 1701
debug avp = no ; enable for debug

[lac exo]
lns = 10.38.140.225
redial = yes
redial timeout = 5
autodial = yes
ppp debug = no ; enable for debug
pppoptfile = /etc/ppp/options.xl2tpd
```

En el fitxer `/etc/ppp/chap-secrets` afegiu el següent contingut (poseu les vostres credencials entre cometes)

```
"elvostreusuari" * "elvostrepassword" *
```

En el fitxer `/etc/ppp/options.xl2tpd` hi haurà el següent contingut(nota: conté un altre cop l'usuari entre cometes):

```
# optional parameter only available through a custom patch from suse and copied in debian (thanks pespin) https://comp.protocols.ppp.narkive.com/E29wCWqW/unrecognizecd-option-replacedefaultroute
#replacedefaultroute
# "noauth" option is mandatory for accelppp (if not says "peer refused to authenticate")
#   -> src https://serverfault.com/questions/418830/pppoe-connection-generic-error-rp-pppoe-child-pppd-process-terminated/419003#419003
noauth
# l2tp user
name "elvostreusuari"
defaultroute
persist
maxfail 0
# Don't wait for LCP term responses; exit immediately when killed.
lcp-max-terminate 0
noproxyarp
mtu 1420
mru 1420
```

i a continuació reiniciar el servei `service xl2tpd restart`

*Treure el següent comentari després de 2020-12-31*. Afectació als usuaris de jessie o de la versió xl2tpd `1.3.6+dfsg-2`; hi ha un error quan s'utilitza l'opció `maxfail 0` i és que la CPU es posa al 100% queda el procés `pppd` enganxat. La solució és comentar o treure aquesta opció, el valor per defecte (`maxfail 10`) ja és prou bo. Fonts que descriuen l'error:

- https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=606458
- https://bugzilla.altlinux.org/show_bug.cgi?id=29676
- https://github.com/xelerance/xl2tpd/issues/68
- https://serverfault.com/questions/834179/xl2tpd-pppd-tunnel-persist-error

## instruccions extra VyOS o EdgeOS

Notes preliminars:

- Per utilitzar edgemax com un debian executar a l'inici `sudo su` i per manegar el servei o aplicar els canvis `/etc/init.d/xl2tpd restart` enlloc de `service xl2tpd restart`.
- No s'ha provat si es mantenen les dades en cas d'actualització. Queda pendent proves al respecte i si hi ha forma de fixar la configuració.

si volem activar xl2pd (el client l2tp) a l'inici d'operació del router aplicar dos comandes. La primera genera els links de start, stop, etc. del servei amb sysvinit

    update-rc.d xl2tpd defaults

la segona activa el servei

    update-rc.d xl2tpd enable

les accions contràries que farien revertir els dos canvis anteriors, és a dir, tornar a la situació inicial. Eliminar els links:

    # src https://askubuntu.com/questions/9382/how-can-i-configure-a-service-to-run-at-startup/574054#574054
    update-rc.d -f xl2tpd remove

desactivar l'inici del servei

    update-rc.d xl2tpd disable

si la connexió ve proveïda per dhclient potser no volem que ens posi el default gateway (canviar eth0 segons la interfície que utilitzem)

    set interfaces ethernet eth0 dhcp-options default-route no-update

la connexió de guifi ha d'anar a part si no, no funcionarà (canviar 192.168.1.1 segons correspongui)

    set protocols static route 10.0.0.0/8 next-hop 192.168.1.1

Ja que no podem configurar el [MSS](https://en.wikipedia.org/wiki/Maximum_segment_size) a través del firewall del sistema ja que estem utilitzant una funcionalitat per darrere. Així doncs a falta d'opció millor hem d'activar el MSS a tot arreu:

    set firewall options mss-clamp interface-type all
    set firewall options mss-clamp mss 1440

Per tenir un backup de la configuració afegida es recomana posar-ho en el directori que es té en compte en backups de configuració, el `/config`:

    mkdir /config/etc-backup
    cp -pr /etc/xl2tpd /config/etc-backup
    cp -pr /etc/ppp /config/etc-backup

after upgrade you have to re-enter configuation; if you want to take profit from backup settings:

    cp -p /etc/xl2tpd/xl2tpd.conf  /etc/xl2tpd/xl2tpd.conf.bak
    cp -p /config/etc-backup/xl2tpd/xl2tpd.conf /etc/xl2tpd/
    cp -p /config/etc-backup/ppp/options.xl2tpd /etc/ppp/
    cp -p /config/etc-backup/ppp/chap-secrets /etc/ppp/
    update-rc.d xl2tpd defaults
    update-rc.d xl2tpd enable
