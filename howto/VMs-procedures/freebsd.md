
- Do not use ZFS inside a VM, it works badly
- Do not enable qemu-agent, with qemu-agent enabled, it tries to send the shutdown signal through it, without it, it does it through acpi. Right now, there is no clear support for qemu-agent
- Hot plugged virtio interfaces are not seen, `reboot`
- Network configuration cannot be reload, you should restart:
  - restart everything `service netif restart`
  - restart specific iface `service netif restart vtnet0`
- Do not change default settings of `/root/.cshrc` or its shell, if you want bash, install bash and use it when needed
  - to login directly with bash, use `ssh -t yourHost bash`
- From FreeBSD side, to enable serial connection add in file `/boot/load.conf` line `console="comconsole"`
- Boot by default takes 10 seconds in initial boot menu, to modify it go to file `/boot/load.conf` and add line `autoboot_delay="1"`

mini rosetta code linux-freebsd ([more](https://github.com/AnotherKamila/linux2bsd))

| description | linux | freebsd |
| ----------- | ----- | ------- |
| show main routing table for IPv4 | ip r | netstat -4rW |
| show main routing table for IPv6 | ip -6 r | netstat -6rW |
| show application of routing table for targetHost | ip route get targetHost | route show targetHost |
