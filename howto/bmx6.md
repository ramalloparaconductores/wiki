# bmx6

## troubleshooting

### in general

when you do specific changes for a device, please, add in `/etc/banner` a short and clear notice to help other maintainers understand that this particular node has custom configurations to take them in account in future upgrades. For example, this `/etc/banner`

```
  _______                     ________        __
 |       |.-----.-----.-----.|  |  |  |.----.|  |_
 |   -   ||  _  |  -__|     ||  |  |  ||   _||   _|
 |_______||   __|_____|__|__||________||__|  |____|
          |__| W I R E L E S S   F R E E D O M
 -----------------------------------------------------
 OpenWrt 18.06.4, r7808-ef686b7292
 -----------------------------------------------------

warning:

- custom /etc/rc.local
```

when I log in, it says it clearly

```
 $ ssh root@10.1.192.97
Warning: Permanently added '10.1.192.97' (RSA) to the list of known hosts.


BusyBox v1.28.4 () built-in shell (ash)

  _______                     ________        __
 |       |.-----.-----.-----.|  |  |  |.----.|  |_
 |   -   ||  _  |  -__|     ||  |  |  ||   _||   _|
 |_______||   __|_____|__|__||________||__|  |____|
          |__| W I R E L E S S   F R E E D O M
 -----------------------------------------------------
 OpenWrt 18.06.4, r7808-ef686b7292
 -----------------------------------------------------

warning:

- custom /etc/rc.local

 Temba 87e188e
 -----------------------------------------------------
```

### br-lan interface is not getting up

when the openwrt with bmx6 is booting up, the interface br-lan is not setting up in a nanostation5 m xw, but doing `/etc/init.d/bmx6 restart` works, then, let's add a delay. In `/etc/rc.local` add:

```
# without this line in this particular node, bmx6 cannot be used in br-lan
sleep 20 && /etc/init.d/bmx6 restart
```

### an unstable link is making a bmx6 node to work very bad

add in `/etc/firewall.user`:

```
# Palencia43 link is very bad, if the other one fails, use Palencia43 as backup
SCGGeneralitat91Rd1="fe80::7a8a:20ff:fecc:ece2"
BCNPalencia43Rd3="fe80::618:d6ff:feee:52f4"
if bmx6 -c links | grep -q "$SCGGeneralitat91Rd1"; then
  ip6tables -A INPUT -i wif0_t -s "$BCNPalencia43Rd3" -j DROP -m comment --comment "don't connect to BCNPalencia43Rd3"
fi
```

and then

   /etc/init.d/firewall restart

a user that sees that the link is not working or is not working properly can reboot the node to refresh the new situation
