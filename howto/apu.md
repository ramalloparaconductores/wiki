# apu2 open source router

## where to buy

according to the [apu2 page](https://pcengines.ch/apu2.htm) as of 2019-3-5 last interesting model is [apu2d4](https://pcengines.ch/apu2d4.htm)

- [Varia link](https://varia-store.com/en/produkt/33035-pc-engines-apu2d4-system-board-3x-lan-4-gb-ram.html) where you can buy all basics (motherboard, case, mSATA or SD, power supply). Comes from Germany.
- [Landashop link](https://www.landashop.com/cmp-apu-2d4.html) where you only get the motherboard part (you have to buy the other parts separately)

## hardware installation

assembly instructions:

- text: [cooling official instructions](https://pcengines.ch/apucool.htm)
- video: [PC Engines APU2/APU3 system assembly. Build your own open source router!](https://www.youtube.com/watch?v=ft_Ic2ZdLHw)

## software installation

prepare:

- a usb with debian installation on it
- a serial console thing `sudo picocom -b 115200 /dev/ttyUSB0`

then plug in apu2

1. Use arrows to focus to menu item "Install" (no graphical)
2. Press `TAB` and append at the end of the command `console=ttyS0,115200n8` and press `ENTER`
3. When says that cannot find graphical card press `SPACE`

extra sources:

- [place where I extracted the arguments to add at boot](https://a.custura.eu/post/debian-via-serial-console/)
- video of full process [Installing Debian on a PC Engines apu2 c4](https://www.youtube.com/watch?v=wnGh0RCMC1c)
- adjust vim lines `:set lines=34` src https://superuser.com/questions/598877/vi-only-show-16-lines-how-to-show-full-screen
- recommended to buy https://www.varia-store.com/en/produkt/33035-pc-engines-apu2d4-system-board-3x-lan-4-gb-ram.html with mSATA 30GB
