# Configure Uninterruptible power supply (UPS) devices

<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [Devices](#devices)
  - [Salicru SPS SOHO+ 850 VA](#salicru-sps-soho-850-va)
  - [Eaton Ellipse ECO 1600](#eaton-ellipse-eco-1600)
- [GUI interfaces](#gui-interfaces)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

In [nut](https://networkupstools.org/) we trust!

## Devices

### Salicru SPS SOHO+ 850 VA

https://www.salicru.com/sps-soho.html

(all commands as root)

install dependencies

    apt install nut nut-client nut-server

configure files:

- in file `/etc/nut/nut.conf` insert `MODE=standalone`
- in file `/etc/nut/ups.conf` insert delete or comment line at the end `maxretry = 3` (Fatal error: 'maxretry' is not a valid variable name for this driver.):
```
[salicru]
driver = usbhid-ups
port = auto
```
- in file `/etc/nut/upsd.conf` insert:
```
MAXAGE 15
STATEPATH /var/run/nut
LISTEN 127.0.0.1 3493
MAXCONN 1024
```
- in file `/etc/nut/upsd.users` insert:
```
[admin]
password = admin
actions = SET
instcmds = ALL
upsmon master
```
- in file `/etc/nut/upsmon.conf` insert:
```
MONITOR salicru@127.0.0.1:3493 1 admin admin master
 # next commands not checked if really needed
NOTIFYCMD /usr/sbin/upssched

 # this was uncommented
NOTIFYMSG ONLINE    "UPS %s on line power"
NOTIFYMSG ONBATT    "UPS %s on battery"

 # this is needed for sure (put that states to exec, to send script-alert) -> src http://freekode.org/nut-sending-emails/
NOTIFYFLAG ONLINE   SYSLOG+WALL+EXEC
NOTIFYFLAG ONBATT   SYSLOG+WALL+EXEC
```
- in file `/etc/nut/upssched.conf` insert:
```
CMDSCRIPT /usr/local/bin/salicru_sched.sh

 # this was uncommented and it might have security implications
PIPEFN /var/run/nut/upssched/upssched.pipe
LOCKFN /var/run/nut/upssched/upssched.lock

 # this is an insert (schedule) -> src https://unix.stackexchange.com/questions/164788/run-various-shell-commands-when-nut-reports-a-low-ups-battery/202184#202184
AT ONBATT * EXECUTE emailonbatt
 ## Five minutes after the UPS goes on battery, run /bin/your-script.sh upsonbatt.
 #AT ONBATT * START-TIMER upsonbatt 300
 # Immediately after the UPS regains line power, run /bin/your-script.sh upsonline.
AT ONLINE * EXECUTE emailonline
```
- the script `/usr/local/bin/salicru_sched.sh` used by upssched is:
```
#!/bin/bash

 # to be able to send mails to an IP you have to configure your [MTA](https://en.wikipedia.org/wiki/Message_transfer_agent)
 # if you use postfix, add this option `postconf -p "resolve_numeric_domain = yes"`
mail="user@192.168.1.2"
hostname="myhost"
timestamp="$(date +"%A %Y-%m-%d %H:%M")"

case $1 in
  emailonbatt)
    echo "$timestamp $hostname: UPS on battery power" | mail -s "[$hostname] UPS on battery power" $mail
    ;;
  emailonline)
    echo "$timestamp $hostname: UPS on line power" | mail -s "[$hostname] UPS on line power" $mail
    ;;
esac
```

restart nut server and client:

```
service nut-server restart
service nut-client restart
```

operations on the device (thanks https://www.jormc.es/2014/05/11/raspi-conectando-un-sai-salicru-sps-one-900va/):

- check driver is loaded succesfully `upsdrvctl start`
- check the data from the UPS device `upsc salicru@127.0.0.1:3493`
- restart nut server `/etc/init.d/nut-server restart` and client `/etc/init.d/nut-client restart` when you change configuration files

Ideas to discover driver by example. In my case dmesg said `hid-generic <masked id>: hiddev1,hidraw2: USB HID v1.00 Device [PPC Offline UPS] on usb-<masked-id>/input0`, other salicru guides were suggesting another driver but it was not working. I looked `lsusb -v` and said `Phoenixtec Power Co., Ltd` from there [I got to this url](https://alioth-lists.debian.net/pipermail/nut-upsuser/2014-December/009403.html) that gave me the idea to try `usbhid-ups`

[archlinux wiki article](https://wiki.archlinux.org/index.php/Network_UPS_Tools) says

> For many UPS connected by USB, use the usbhid-ups(8) driver. For UPS with serial port, use port=/dev/ttySX, where X is the number of serial port (Example:/dev/ttyS1). For UPS with USB port, use port=auto.

All nut drivers are here `/lib/nut` you can also [check compatibility (drivers mapped to devices) here](https://networkupstools.org/stable-hcl.html)

### Eaton Ellipse ECO 1600

According to the [hardware compatibility list of nut wiki](https://networkupstools.org/stable-hcl.html) looks like they have greater support than salicru

http://www.bernaerts-nicolas.fr/linux/75-debian/335-debian-wheezy-install-monitor-eaton-ups

## GUI interfaces

assuming standalone-localhost

GUI application (probably requires a graphical desktop running)

    apt install nut-monitor

run it with

    NUT-monitor

web-based application (depends on apache2)

    apt install nut-cgi

in file `/etc/nut/hosts.conf` add `MONITOR mydevice@localhost 'Must PowerAgent 1060'` (where my device is the name you put in `ups.conf` file, for example if you put [salicru] then mydevice is salicru (thanks http://adi.roiban.ro/articles/2011/2011-10-10-nut.html)

visit webpage http://localhost/cgi-bin/nut/upsstats.cgi
