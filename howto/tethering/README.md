[Tethering](https://en.wikipedia.org/wiki/Tethering) means sharing of a mobile device's Internet connection with other connected computers

- procedure with [android](android.md)
  - you can delegate the connection sharing to an appropriate system such as openwrt that could give connection to ethernet cables, wifi, maybe a vpn client, etc.
    - [tethering with openwrt](https://openwrt.org/docs/guide-user/network/wan/smartphone.usb.tethering) - tested, works
    - [reverse tethering with openwrt](https://openwrt.org/docs/guide-user/network/wan/smartphone.usb.reverse.tethering) - not tested, looks nice
