This guide explains how to connect your mobile phone (android) to the computer (linux) to share internet connection available from the mobilephone which it can switch from wifi to data (3g or 4g). This method is known as [tethering](https://en.wikipedia.org/wiki/Tethering)

First of all, connect your phone to laptop with an appropriate usb cable

Then, in the mobile phone follow next steps:

Go to *Apps*:

![](android-0.png)

Go to *Settings*:

![](android-1.png)

Go to *Connections*:

![](android-2.png)

Go to *shared connection / modem*:

![](android-3.png)

Enable modem USB (it only allows you to enable it if you first connected the mobile phone to the computer):

![](android-4.png)

in a linux laptop, using `sudo dmesg` we see the following sequence that shows how first the mobile phone is detected and when we enable the *modem USB* mode, detects an ethernet interface, and from there we can do `sudo dhclient enp0s20u2` for our example (we can double check that in `sudo ip link` there is that interface)

```
usb 1-2: new high-speed USB device number 6 using xhci_hcd
usb 1-2: New USB device found, idVendor=<masked2>, idProduct=<masked3>, bcdDevice= 4.00
usb 1-2: New USB device strings: Mfr=2, Product=3, SerialNumber=4
usb 1-2: Product: SAMSUNG_Android
usb 1-2: Manufacturer: SAMSUNG
usb 1-2: SerialNumber: <masked1>
usb 1-2: USB disconnect, device number 6
usb 1-2: new high-speed USB device number 7 using xhci_hcd
usb 1-2: New USB device found, idVendor=<masked2>, idProduct=masked3>, bcdDevice= 4.00
usb 1-2: New USB device strings: Mfr=2, Product=3, SerialNumber=4
usb 1-2: Product: SAMSUNG_Android
usb 1-2: Manufacturer: SAMSUNG
usb 1-2: SerialNumber: <masked1>
usb 1-2: USB disconnect, device number 7
usb 1-2: new high-speed USB device number 8 using xhci_hcd
usb 1-2: New USB device found, idVendor=04e8, idProduct=6863, bcdDevice=ff.ff
usb 1-2: New USB device strings: Mfr=2, Product=3, SerialNumber=4
usb 1-2: Product: SAMSUNG_Android
usb 1-2: Manufacturer: SAMSUNG
usb 1-2: SerialNumber: <masked1>
usbcore: registered new interface driver cdc_ether
rndis_host 1-2:1.0 usb0: register 'rndis_host' at usb-0000:00:14.0-2, RNDIS device, 01:02:03:04:05:06
usbcore: registered new interface driver rndis_host
rndis_host 1-2:1.0 enp0s20u2: renamed from usb0
IPv6: ADDRCONF(NETDEV_UP): enp0s20u2: link is not ready
```
