<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [legal](#legal)
  - [Sobre potencias y frecuencias del wifi](#sobre-potencias-y-frecuencias-del-wifi)
  - [plans de desplegament i ajuntaments](#plans-de-desplegament-i-ajuntaments)
  - [modelos de comunicación](#modelos-de-comunicaci%C3%B3n)
    - [modelo de comunicación con vecinos (sencillo)](#modelo-de-comunicaci%C3%B3n-con-vecinos-sencillo)
    - [modelo de comunicación (completo)](#modelo-de-comunicaci%C3%B3n-completo)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# legal

## Sobre potencias y frecuencias del wifi

[Ver el documento aquí](./wififrecuenciaspotencias.pdf)

## plans de desplegament de telecomunicacions i ajuntaments

Arriba el missatge per part d'un ajuntament com

> Es posa en coneixement de la població, que actualment l'únic operador autoritzat per l'Ajuntament de *x* per al desplegament de la fibra òptica al nostre municipi és *y*.

Resposta: Aquest fet és denunciable. Els Ajuntaments no autoritzen plans de desplegament, nomes els aproben, i han de justificar els motius.

més info amb un exemple: https://guifi.net/ca/node/109784

## modelos de comunicación

### modelo de comunicación con vecinos (sencillo)

```
En ______, a ___________ de ____

Att. Comunidad de Vecinos

__________________ con DNI _______ propietario del piso ____, portal ____ de la Calle
_____________ en ________________ solicita la autorización pertinente para la ubicación de una
equipo de telecomunicaciones en la zona comunitaria habilitada a tal efecto, según se regula
en el R.D. 1/1998, sobre infraestructuras comunes en los edificios para el acceso a los servicios
de comunicaciones de 27.02.1998 (BOE núm. 51, de 28.02.1998) en su Art. 9. Dicho equipo, de
unos 40cm de diámetro y de menos de 2kg de peso, opera en tecnología de radio, sobre
frecuencias libres (5ghz).

La instalación sería ejecutada a cargo de personal cualificado y debidamente homologado
para ello, utilizando para ello los mástiles existentes, dadas sus escasas dimensiones, o
habilitando algún soporte al efecto, sin incidir o afectar, en ningún caso, sobre el resto de
servicios de telecomunicaciones presentes en el Edificio.
Sin más, esperando su respuesta,
Un saludo,
Firma: ________________
Notificado el día __________
Firma del Presidente/a o Secretario/a de la Comunidad de Propietarios
Firma: ___________
```

src https://llistes.guifi.net/sympa/arc/guifi-users/2018-07/msg00011.html

### modelo de comunicación (completo)

En ocasiones nos encontramos con administradores sin demasiado conocimiento de la legislación o un recelo excesivo y se aferran al artículo 17.1 de la ley 49/1960 sobre la Propiedad Horizontal o una supuesta contradicción entre esta ley y el RDL 1/1998, así que decidimos incluir copias de los articulos relativos a la instalación y una pequeña explicacion de la interpretación de dichas leyes.

A partir de que empezamos a presentar la txapa de solicitud, no nos han vuelto a poner pegas.

[Ver el documento aquí](./InstalacionAntenaEdificio.doc)

### extras

Sobre els permisos amb terceres parts: Recorda que com a propietari d'un pis tens dret a posar aquella infraestructura en espai comú per poder accedir a un servei de telecomunicacions del que la teva comunitat no està dotada (www.boe.es/boe/dias/1998/02/28/pdfs/A07071-07074.pdf). -> src https://guifi.net/ComunsXOLN#Sobre_la_Xarxa
