exchange directory, in this example `/nfs`, between two hosts in a LAN

# server

install nfs server

    apt install nfs-kernel-server

put in file `/etc/exports`

    /nfs 192.168.95.0/24(rw,sync,no_subtree_check,no_root_squash)

*supposing that 192.168.95.0/24 is the network shared among servers*

reload nfs shares

    exportfs -arv

# client

install nfs client

    apt install nfs-common

put in file `/etc/fstab`

192.168.95.2:/nfs /nfs nfs rw,async,hard,intr,noexec 0 0

reload `/etc/fstab` config

    mount -a

*supposing that 192.168.95.1 is the server that hosts the nfs server*

notes:

- you can have as many hosts accessing the files of nfs server as you want
- if you want to have write permissions in the nfs share with a non-root user you need to have the same user on the server (or map appropiately the uid and gid), as a bad designed solution, using the following command works: `chmod 777 -R yournfsvolume`
