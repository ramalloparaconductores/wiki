# Netflow

NetFlow is a feature that was introduced on Cisco routers around 1996 that provides the ability to collect IP network traffic as it enters or exits an interface. By analyzing the data provided by NetFlow, a network administrator can determine things such as the source and destination of traffic, class of service, and the causes of congestion. (extracted from wikipedia.org: https://en.wikipedia.org/wiki/NetFlow).

Although initially was a feature only present in Cisco network devices nowadays there are similar (and mostly compatible) implementations in other devices.

In this article we will cover data exposition (using a MikroTik router), data extraction (using flow-tools) and data processing/viewing (using cacti FlowView plugin).

## Architecture and protocol basis
TODO: Explain how netflow works in low level.

## Software installation
### flow-tools
flow-tools is the software used to extract data from the network devices we want to monit.

We have used an LXC container with Debian Stretch to deploy the service.


### FlowView plugin installation
