# global vars
if [[ -z "$ENV_FILE" ]]; then
  source non-interactive-install-env
else
  source "$ENV_FILE"
fi
# _slapd_conf functions
source non-interactive-install-functions-conf.sh
# _slapd_add functions
source non-interactive-install-functions-add.sh
# printing style (cprint)
source style.sh
