# thanks evilham
color1=$(tput setaf 3)
color2=$(tput setaf 6)
N=$(tput sgr0)

cprint() {
  # style
  if [[ -z $2 ]]; then
    pre="$color1\n  "
    post="\n"
  else
    pre="$color2  "
    post=""
  fi

  echo -e "${pre}${1}${post}${N}"
}
