cprint 'populate LDAP with example groups and users'
# core part (users, services, ldap-admins)
_slapd_add_general_group 'users'
_slapd_add_general_group 'groups'
# ldap-admins group matches the previous defined acl to allow special users to control ldap
_slapd_add_group 'ldap-admins'

# populating LDAP facilitates the nextcloud LDAP detection and configuration
# it is also a good example

_slapd_add_user 'test-user1' "$USER_TEST_PASSWD"
_slapd_add_user 'test-user2' "$USER_TEST_PASSWD"
_slapd_add_user 'test-user3' "$USER_TEST_PASSWD" "custom-email@example.org"

_slapd_add_group 'test-group1'
_slapd_add_group 'test-group2'

_slapd_add_user_to_group 'test-user1' 'ldap-admins'
_slapd_add_user_to_group 'test-user1' 'test-group1'
_slapd_add_user_to_group 'test-user2' 'test-group1'
_slapd_add_user_to_group 'test-user3' 'test-group2'
