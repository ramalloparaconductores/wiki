<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

  - [Intro](#intro)
  - [Nextcloud configuration](#nextcloud-configuration)
  - [ApacheDS access](#apacheds-access)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

## Intro

@buttle (Chris) did a great technical documentation here explaining how to setup an LDAP for a small organization. You can find it [here](README-orig.md)

Here I would like to explain it in a more general manner

This LDAP service ([slapd](https://www.openldap.org/))is intended to be used in a small organization where we have:

- a group of **users** to know what users are part of the organization
- a group of **services** to know what users have speical access to restricted or special services (a part of the common services for all)
- TODO a group of service users to bind specific services with LDAP connector to this LDAP service
  - check if yunohost does it this way
  - chris used same cn=nobody for all services
  - ACLs so that bind user of service can only have just the related data to operate

Why this is so great?

- slapd installation, configuration and db population is very simple, you can do it in your laptop with `sudo ./non-interactive-install`
- management of users and groups can be managed with nextcloud, which looks like a decent WebGUI (thanks @evilham)

Important security notes:

- Right now, I don't feel this is hardened enough to be in a public internet:
  - fixme: enforcing TLS is problematic
  - fixme: people complain the default ACLs from slapd in debian, and this provides only a small modification. Hence, it is not verified and it could leak information to unauthorized people
- Nextcloud it is in [PHP](https://en.wikipedia.org/wiki/PHP#Security), but the service is easy to upgrade

## Nextcloud configuration

Requirements:

- debian buster 10 in order to have php 7.1+
- tested in nextcloud 18.0.1

[Check installation steps here](nextcloud.md)

Enable the plugin:

- `apt install php7.3-ldap`
- reload webserver
  - apache2 case `systemctl reload apache2`
  - nginx with php-fpm case `systemctl reload php-fpm7.3`
- enable plugin `LDAP user and group backend` (tested version 1.8.0)
  - that enables `Write support for LDAP` (tested version 1.1.0)

And then enter to the server with ssh to setup ldap configuration.

Assuming nextcloud path is `/var/www/html/nextcloud/` for the next optional two commands

(optional) Delete wrong or previous configs (s01, s02, etc.):

    su www-data -s /bin/sh -c 'php /var/www/html/nextcloud/occ ldap:delete-config s02'

(optional) Create empty configuration file

    su www-data -s /bin/sh -c 'php /var/www/html/nextcloud/occ ldap:create-empty-config'

then run `./gen-config-nextcloud.sh` (customize variables if required) and execute the output file `config-nextcloud.sh` in your server

In Settings / (Administration section) LDAP / AD integration, verify that we are using the appropiate configuration (s01 if is a new installation).

`Writing` section:

- `Prevent fallback to other backends when creating users or groups.` marked
- `To create users, the acting (sub)admin has to be provided by LDAP.` marked
- `A random user ID has to be generated, i.e. not being provided by the (sub)admin.` unmarked
  - not recommended, because uid looks ugly when looking it with ApacheDS
- `An LDAP user must have an email address set.` marked
- `Allow users to set their avatar` marked

And at the end of the `User template` looks like:

```
dn: uid={UID},ou=users,{BASE}
objectClass: inetOrgPerson
uid: {UID}
displayName: {UID}
cn: {UID}
sn: {UID}
userPassword: {PWD}
```

**don't put a newline character in the end** or then you cannot add users. [bug reported](https://github.com/nextcloud/ldap_write_support/issues/132)

Nextcloud by default avoid collisions between LDAP and internal users, but you can override this behavior ([which is useful sometimes](https://help.nextcloud.com/t/migration-to-ldap-keeping-users-and-data/13205)):

    su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapExpertUsernameAttr uid'

## ApacheDS access

A good LDAP admin will ask you for access with [ApacheDS](https://directory.apache.org/apacheds/)

- Accessing the DIT (tree), click `New connection`
  - Network Parameter (First menu)
    - connection name: `$ORG_DNS tree` (I recommend this name)
    - hostname: `ORG_DNS`
    - port: `636`
    - encryption method: `Use SSL encryption (ldaps://)`
    - note: if you click `Check Network Parameter` it will fail, **but click next**
  - Authentication
    - Bind DN or user: `cn=admin,$ORG_DN`
    - Bind password: `$PASSWD`
    - note: if you click `Check Network Parameter` should work
    - Click `Finish`
- Accessing the `cn=config`, click `New connection`, same parameters, except two:
  - Network Parameter (First menu)
    - connection name: `$ORG_DNS config` (I recommend this name)
  - Authentication
    - Bind DN or user: `cn=admin,cn=config`
